#ifndef SECONDCLASS_H
#define SECONDCLASS_H
#include "ExampleClasses_global.h"
#include <QSharedPointer>
#include <BaseClass.h>

class IncludedInSecondClass_1 : public BaseClass2
{
public:
	QString m_data1;
	int m_data2;
	double m_data3;

	IncludedInSecondClass_1() {}
	virtual ~IncludedInSecondClass_1() {}
};
TEST_HPP_REG (IncludedInSecondClass_1, BaseClass2, 0)

class SecondClass
{
public:
	QString m_id;
	QString m_someData = "Data1";
	double m_someData2;
	QString m_someData3;
	IncludedInSecondClass_1 m_anObj;
	QSharedPointer<IncludedInSecondClass_1> m_anotherObj;
	QList<QSharedPointer<IncludedInSecondClass_1>> m_listOfAnotherObj;

	SecondClass() {}
	virtual ~SecondClass() {}
};
TEST_PRIMARY_KEY_QSTRING (SecondClass)
TEST_HPP_NOBASE_REG (SecondClass)

#endif		  // SECONDCLASS_H
