#include "FirstClass.h"

TEST_CPP_REG (FirstClass)

namespace qx
{
template <> void register_class (QxClass<FirstClass> &t)
{
	t.id (&FirstClass::m_id, "_id");
	auto relIdField = t.data (&FirstClass::m_secondClass, "secondClass");
	t.relationOneToOne (&FirstClass::m_secondClassObj, "secondClassObj")->linkRelationKeyTo (relIdField);
	t.data (&FirstClass::m_scSomeData, "scSomeData");
	t.data (&FirstClass::m_scSomeData2, "scSomeData2");
	t.data (&FirstClass::m_scSomeData3, "scSomeData3");
	t.data (&FirstClass::m_scInclData2, "scInclData2");

	t.data (&FirstClass::m_date, "date");
	t.data (&FirstClass::m_dateTime, "dateTime");
	t.data (&FirstClass::m_Time, "Time");

	t.setPropertyBag ("Is FinalDatabaseClass", true);

	QVariantHash validators;
	QJsonObject valScOid, valScSomeData, valScSomeData2, valScSomeData3, valScInclData2;

	{
		QJsonObject eodObj;
		eodObj.insert ("collection", "SecondClass");
		eodObj.insert ("local field for relation", "secondClass");
		eodObj.insert ("remote field", "_id");
		eodObj.insert ("remote field type", "id");
		eodObj.insert ("create completer", false);
		QJsonArray propagArr = QJsonArray{} << "scSomeData"
											<< "scInclData2"
											<< "scSomeData2"
											<< "scSomeData3";
		eodObj.insert ("propagate to fields", propagArr);
		valScOid.insert ("equal other doc", eodObj);
	}
	valScOid.insert ("type", QJsonArray{} << "equal other doc");

	{
		QJsonObject eodObj;
		eodObj.insert ("collection", "SecondClass");
		eodObj.insert ("local field for relation", "scSomeData");
		eodObj.insert ("remote field", "someData");
		eodObj.insert ("remote field type", "data");
		eodObj.insert ("create completer", true);
		QJsonArray propagArr = QJsonArray{} << "secondClass"
											<< "scInclData2"
											<< "scSomeData2"
											<< "scSomeData3";
		eodObj.insert ("propagate to fields", propagArr);
		valScSomeData.insert ("equal other doc", eodObj);
		valScSomeData.insert ("forbidden characters", QJsonArray{} << '_' << '-' << ' ');
	}
	valScSomeData.insert ("type",
		QJsonArray{} << "equal other doc"
					 << "forbidden characters");

	{
		QJsonObject eodObj;
		eodObj.insert ("collection", "SecondClass");
		eodObj.insert ("local field for relation", "scSomeData");
		eodObj.insert ("remote field for relation", "someData");
		eodObj.insert ("remote field", "someData2");
		eodObj.insert ("remote field type", "data");
		eodObj.insert ("create completer", true);
		QJsonArray propagArr = QJsonArray{} << "secondClass"
											<< "scInclData2"
											<< "scSomeData"
											<< "scSomeData3";
		eodObj.insert ("propagate to fields", propagArr);
		valScSomeData2.insert ("equal other doc", eodObj);
	}
	valScSomeData2.insert ("type", QJsonArray{} << "equal other doc");

	{
		QJsonObject eodObj;
		eodObj.insert ("collection", "SecondClass");
		eodObj.insert ("local field for relation", "scSomeData3");
		eodObj.insert ("remote field for relation", "someData3");
		eodObj.insert ("remote field", "someData3");
		eodObj.insert ("remote field type", "data");
		eodObj.insert ("create completer", true);
		QJsonArray propagArr = QJsonArray{} << "secondClass"
											<< "scInclData2"
											<< "scSomeData2"
											<< "scSomeData";
		eodObj.insert ("propagate to fields", propagArr);
		valScSomeData3.insert ("equal other doc", eodObj);
	}
	valScSomeData3.insert ("type", QJsonArray{} << "equal other doc");

	{
		QJsonObject eodObj;
		eodObj.insert ("collection", "SecondClass");
		eodObj.insert ("local field for relation", "scInclData2");
		eodObj.insert ("remote field", "anObj.data2");
		eodObj.insert ("remote field type", "data");
		eodObj.insert ("create completer", true);
		QJsonArray propagArr = QJsonArray{} << "scSomeData"
											<< "secondClass"
											<< "scSomeData2"
											<< "scSomeData3";
		eodObj.insert ("propagate to fields", propagArr);
		valScInclData2.insert ("equal other doc", eodObj);
	}
	valScInclData2.insert ("type", QJsonArray{} << "equal other doc");

	validators.insert ("secondClass", valScOid);
	validators.insert ("scSomeData", valScSomeData);
	validators.insert ("scInclData2", valScInclData2);
	validators.insert ("scSomeData2", valScSomeData2);
	validators.insert ("scSomeData3", valScSomeData3);
	t.setPropertyBag ("validators", validators);
}
}		 // namespace qx
