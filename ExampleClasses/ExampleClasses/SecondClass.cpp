#include "SecondClass.h"
TEST_CPP_REG (IncludedInSecondClass_1)

namespace qx
{
template <> void register_class (QxClass<IncludedInSecondClass_1> &t)
{
	t.data (&IncludedInSecondClass_1::m_data1, "data1");
	t.data (&IncludedInSecondClass_1::m_data2, "data2");
	t.data (&IncludedInSecondClass_1::m_data3, "data3");
}
}		 // namespace qx

TEST_CPP_REG (SecondClass)

namespace qx
{
template <> void register_class (QxClass<SecondClass> &t)
{
	t.id (&SecondClass::m_id, "_id");
	t.data (&SecondClass::m_someData, "someData");
	t.data (&SecondClass::m_someData2, "someData2");
	t.data (&SecondClass::m_someData3, "someData3");
	t.data (&SecondClass::m_anObj, "anObj");
	t.data (&SecondClass::m_anotherObj, "anotherObj");
	t.data (&SecondClass::m_listOfAnotherObj, "listOfAnotherObj");
	t.setPropertyBag ("Is FinalDatabaseClass", true);
}
}		 // namespace qx
