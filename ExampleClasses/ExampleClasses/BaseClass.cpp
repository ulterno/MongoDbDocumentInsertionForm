#include "BaseClass.h"

TEST_CPP_REG (BaseClass)
TEST_CPP_REG (BaseClass2)

namespace qx
{
template <> void register_class (QxClass<BaseClass> &t)
{
	t.data (&BaseClass::m_baseClassData, "baseClassData");
	t.data (&BaseClass::m_deprecatedDataMember, "deprecatedDataMember");
	t.data (&BaseClass::m_tOrF, "tOrF");

	t.setPropertyBag ("ignore data members", QStringList() << "deprecatedDataMember");
}

template <> void register_class (QxClass<BaseClass2> &t)
{
	t.data (&BaseClass2::m_baseClass2Data, "baseClass2Data");
	t.data (&BaseClass2::m_deprecatedDataMember2, "deprecatedDataMember2");
}
}		 // namespace qx
