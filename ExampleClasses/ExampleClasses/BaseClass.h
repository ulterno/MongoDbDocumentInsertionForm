#ifndef BASECLASS_H
#define BASECLASS_H
#include <ExampleClasses_global.h>

class BaseClass
{
public:
	QString m_baseClassData = "BCD";

	int m_deprecatedDataMember;
	bool m_tOrF;

	BaseClass() {}
	virtual ~BaseClass() {}
};

TEST_HPP_NOBASE_REG (BaseClass)

class BaseClass2
{
public:
	QString m_baseClass2Data = "hfed";

	int m_deprecatedDataMember2;

	BaseClass2() {}
	virtual ~BaseClass2() {}
};

TEST_HPP_NOBASE_REG (BaseClass2)

#endif		  // BASECLASS_H
