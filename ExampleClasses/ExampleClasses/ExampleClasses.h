#ifndef EXAMPLECLASSES_H
#define EXAMPLECLASSES_H

#include "ExampleClasses_global.h"
#include <BaseClass.h>
#include <FirstClass.h>
#include <SecondClass.h>

class EXAMPLECLASSES_EXPORT ExampleClasses
{
public:
	ExampleClasses();
};

#endif		  // EXAMPLECLASSES_H
