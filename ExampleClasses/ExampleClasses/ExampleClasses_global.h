#ifndef EXAMPLECLASSES_GLOBAL_H
#define EXAMPLECLASSES_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QxOrm.h>

#define TEST_DLL_EXP QX_DLL_EXPORT_HELPER
#define TEST_HPP_REG QX_REGISTER_HPP_EXPORT_DLL
#define TEST_HPP_NOBASE_REG(className) QX_REGISTER_HPP_EXPORT_DLL (className, qx::trait::no_base_class_defined, 0)
#define TEST_CPP_REG QX_REGISTER_CPP_EXPORT_DLL

#define TEST_PRIMARY_KEY_TYPE QX_REGISTER_PRIMARY_KEY
#define TEST_PRIMARY_KEY_QSTRING(className) QX_REGISTER_PRIMARY_KEY (className, QString)

typedef QMap<QString, QString> ObjectIdType;
#define GRIDFS_PRIMARY_KEY_OBJID(className) QX_REGISTER_PRIMARY_KEY (className, ObjectIdType)
constexpr char def_ObjectIdType[] = "QMap<QString,QString>";

#if defined(EXAMPLECLASSES_LIBRARY)
#define EXAMPLECLASSES_EXPORT Q_DECL_EXPORT
#else
#define EXAMPLECLASSES_EXPORT Q_DECL_IMPORT
#endif

#endif		  // EXAMPLECLASSES_GLOBAL_H
