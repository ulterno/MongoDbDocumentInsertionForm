QT -= gui

TEMPLATE = lib
DEFINES += EXAMPLECLASSES_LIBRARY

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    BaseClass.cpp \
    ExampleClasses.cpp \
    FirstClass.cpp \
    SecondClass.cpp

HEADERS += \
    BaseClass.h \
    ExampleClasses_global.h \
    ExampleClasses.h \
    FirstClass.h \
    SecondClass.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target

QX_BSON_INCLUDE_PATH=/opt/mongo/drivers/mongoc/include/libbson-1.0
QX_MONGOC_INCLUDE_PATH=/opt/mongo/drivers/mongoc/include/libmongoc-1.0
QX_BSON_LIB_PATH=/opt/mongo/drivers/mongoc/lib/
QX_MONGOC_LIB_PATH=/opt/mongo/drivers/mongoc/lib/

include (/opt/QxOrm/QxOrm.pri)

INCLUDEPATH += /opt/QxOrm/include
LIBS += -L/opt/QxOrm/lib -lQxOrmd
