#ifndef FIRSTCLASS_H
#define FIRSTCLASS_H
#include "BaseClass.h"
#include "SecondClass.h"
#include <QDateTime>
class FirstClass : public BaseClass
{
public:
	QString m_id;

	ObjectIdType m_secondClass;
	QString m_scSomeData;
	int m_scInclData2;
	double m_scSomeData2;
	QString m_scSomeData3;

	QSharedPointer<SecondClass> m_secondClassObj;
	QDate m_date;
	QDateTime m_dateTime;
	QTime m_Time;

	FirstClass() {}
	virtual ~FirstClass() {}
};

TEST_PRIMARY_KEY_QSTRING (FirstClass)
TEST_HPP_REG (FirstClass, BaseClass, 0)

#endif		  // FIRSTCLASS_H
