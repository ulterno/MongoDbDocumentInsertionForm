#include "MainWidget.h"
#include "ui_MainWidget.h"
#include "common.h"
#include <ExampleClasses.h>
#include <QLabel>
#include <QFile>
#include <QDate>
#include <QKeyEvent>
#include <Elements/ObjectPickerDialog.h>

void setDefaultConfig (qx::QxSqlDatabase *dbp)
{
	dbp->setDatabaseName ("test" + QDate::currentDate().toString (Qt::ISODate), true);
	dbp->setHostName ("localhost", true);
	dbp->setConnectOptions ("connectTimeoutMS=10&serverSelectionTimeoutMS=100");
}

MainWidget::MainWidget (QWidget *parent) : QWidget (parent), ui (new Ui::MainWidget)
{
	//	qx::QxClassX::registerAllClasses();
	ExampleClasses nc{};
	ui->setupUi (this);
	setWindowTitle ("Inserting Documents to MongoDB (Press [F5] to Reset)");
	ui->pB_Insert->setDisabled (true);

	ui->pB_SearchFunction->setToolTip ("[F3]");

	qx::QxClassX::dumpAllClasses();

	qx::QxCollection<QString, qx::IxClass *> *pAllClasses = qx::QxClassX::getAllClasses();
	if (!pAllClasses)
	{
		qFatal ("PROBLEM::: Could not get all classes");
	}
	{
		QStringList clssList;
		for (auto iter = pAllClasses->begin(); iter != pAllClasses->end(); ++iter)
		{
			if (iter->second->getPropertyBag ("Is FinalDatabaseClass").toBool() && (iter->second->getId()))
			{
				clssList << iter->first;
			}
		}
		ui->cB_CollectionName->addItems (clssList);
	}

	qx::QxSqlDatabase *dbp = qx::QxSqlDatabase::getSingleton();
	dbp->setDriverName ("QXMONGODB");
	QFile configFile ("config.json");
	if (!configFile.open (QFile::ReadOnly))
	{
		qDebug() << "No config file. Setting default configuration";
		setDefaultConfig (dbp);
	}
	else
	{
		QJsonParseError err;
		const QJsonDocument configDoc = QJsonDocument::fromJson (configFile.readAll(), &err);
		if (err.error)
		{
			qDebug() << "Error parsing config: " << err.errorString() << "setting default config";
			setDefaultConfig (dbp);
		}
		else
		{
			const QJsonObject configObj = configDoc.object();
			dbp->setHostName (configObj["hostname"].toString ("localhost"));
			dbp->setPort (configObj["port"].toInt (27017));
			dbp->setUserName (configObj["username"].toString (""), true);
			dbp->setPassword (configObj["password"].toString (""), true);
			dbp->setDatabaseName (configObj["database"].toString ("test-" + QDate::currentDate().toString (Qt::ISODate)), true);
			if (configObj["options"].isObject())
			{
				QString optString;
				const QJsonObject options = configObj["options"].toObject();
				const QStringList keys = options.keys();
				for (QString key : keys)
				{
					optString += "&" + key + "=" + options[key].toVariant().toString();
				}
				optString.remove (0, 1);
				dbp->setConnectOptions (optString, true);
			}
		}
	}
}

MainWidget::~MainWidget() { delete ui; }

void MainWidget::on_pB_CreateForm_clicked()
{
	if (!ui->pB_CreateForm->isEnabled())
	{
		return;
	}
	ui->gB_CollBox->setTitle (ui->cB_CollectionName->currentText());
	if (!m_valueReturner)
	{
		auto xWid = new ObjectFormMaker (ui->cB_CollectionName->currentText(), ui->cB_CollectionName->currentText());
		m_valueReturner = xWid;
		ui->scrollAreaLayout->addWidget (xWid);
		ui->pB_CreateForm->setDisabled (true);
		ui->pB_Insert->setEnabled (true);
	}
}

void MainWidget::on_pB_Insert_clicked()
{
	if (!ui->pB_Insert->isEnabled())
	{
		return;
	}
	if (m_valueReturner)
	{
		QJsonObject retObj = m_valueReturner->getValueData().toObject();
		if (retObj["_id"].isNull())
		{
			retObj.remove ("_id");
		}

		QString documentToSend = QString::fromUtf8 (QJsonDocument (retObj).toJson());
		// Show the resultant document
		qDebug() << m_valueReturner->getKey();
		qDebug() << retObj;

		qx_query insQuery ("{\"insert\" : \"" + m_valueReturner->getKey() + "\", \"documents\" : [" + documentToSend + "] }");
		QSqlError err = qx::dao::call_query (insQuery);
		if (err.isValid())
		{
			qDebug() << "Error Inserting Data: " << err.text() << err.databaseText() << err.driverText() << err.type();
		}
	}
}

void MainWidget::on_pB_SearchFunction_clicked()
{
	if (!ui->pB_SearchFunction->isEnabled())
	{
		return;
	}
	qx::IxClass *classInfo = qx::QxClassX::getClass (ui->cB_CollectionName->currentText());
	QString idKey = classInfo->getId()->getKey();
	QStringList salient_keys = classInfo->getPropertyBag ("salient keys").toStringList();
	QJsonObject projection;
	for (QString key : salient_keys)
	{
		projection.insert (key, 1);
	}
	qx_query listQuery ("cursor",
		"{\"find\" : \"" + ui->cB_CollectionName->currentText() + "\", \"projection\" : " + QString::fromUtf8 (QJsonDocument (projection).toJson())
			+ " }");
	QSqlError err = qx::dao::call_query (listQuery);
	if (err.isValid())
	{
		qDebug() << "Search failed : " << err.text() << err.databaseText() << err.driverText();
	}
	QJsonParseError jsErr;
	QJsonArray respList = QJsonDocument::fromJson (listQuery.response().toString().toUtf8(), &jsErr).array();
	if (jsErr.error != QJsonParseError::NoError)
	{
		qDebug() << "Unable to parse return document : " << jsErr.errorString();
	}
	/// Create Modal Dialogue to select document
	QString selectedOid = ObjectPickerDialog::getEntrySelection (ui->cB_CollectionName->currentText(), idKey, respList, this);
	qDebug() << "User Selected: " << selectedOid;
	/// Get whole document
	QJsonObject filter;
	QJsonObject oid;
	oid.insert ("$oid", selectedOid);
	QJsonObject inF;
	inF.insert ("$in", QJsonArray() << oid);
	filter.insert (idKey, inF);
	qx_query docQuery ("cursor",
		"{\"find\" : \"" + ui->cB_CollectionName->currentText() + "\", \"filter\" : " + QString::fromUtf8 (QJsonDocument (filter).toJson()) + " }");
	qDebug() << docQuery.query();
	err = qx::dao::call_query (docQuery);
	if (err.isValid())
	{
		qDebug() << "Get Document by OID failed : " << err.text() << err.databaseText() << err.driverText();
	}
	qDebug() << docQuery.response();
	respList = QJsonDocument::fromJson (docQuery.response().toString().toUtf8(), &jsErr).array();
	if (jsErr.error != QJsonParseError::NoError)
	{
		qDebug() << "Unable to parse return document : " << jsErr.errorString();
	}
	if (respList.isEmpty())
	{
		qDebug() << "Empty Result";
		return;
	}
	qDebug() << "Number of documents" << respList.count();
	/// Place Document Contents in UI
	if (!m_valueReturner)
	{
		DataItem::WidgetConfiguration objectConfig{};
		objectConfig.removable = false;
		objectConfig.enableDocumentId = true;
		objectConfig.disableDisable = true;
		objectConfig.object_disableAllTopLevelElements = true;
		auto xWid = new ObjectFormMaker (ui->cB_CollectionName->currentText(), ui->cB_CollectionName->currentText(), objectConfig);
		m_valueReturner = xWid;
		ui->scrollAreaLayout->addWidget (xWid);
		ui->pB_CreateForm->setDisabled (true);
		ui->pB_Insert->setEnabled (true);

		qDebug() << "Adding Data: " << respList.at (0);
		if (!xWid->setValueData (respList.at (0)))
		{
			qDebug() << "Failed Adding data to UI";
		}
	}
	else
	{
		qDebug() << "Adding Data: " << respList.at (0);
		if (!m_valueReturner->setValueData (respList.at (0)))
		{
			qDebug() << "Failed Adding data to UI";
		}
	}
}

void MainWidget::on_pB_Update_clicked()
{
	if (!ui->pB_Update->isEnabled())
	{
		return;
	}
	qx::IxClass *classInfo = qx::QxClassX::getClass (ui->cB_CollectionName->currentText());
	QString idKey = classInfo->getId()->getKey();
	if (m_valueReturner)
	{
		QJsonObject retObj = m_valueReturner->getValueData().toObject();
		if (retObj[idKey].isNull())
		{
			qDebug() << "Cannot Update. No ID";
			return;
		}

		QStringList keys = retObj.keys();
		for (QString key : keys)
		{
			/// Remove all disabled fields
			if (retObj[key].isUndefined() || retObj[key].isNull())
			{
				retObj.remove (key);
			}
		}
		QJsonObject setQueryObject;
		setQueryObject.insert ("$set", retObj);
		QString documentToSend = QString::fromUtf8 (QJsonDocument (setQueryObject).toJson());
		// Show the resultant document
		qDebug() << m_valueReturner->getKey();
		qDebug() << retObj;

		QJsonObject filter;
		QJsonObject inF;
		inF.insert ("$in", QJsonArray() << retObj[idKey]);
		filter.insert (idKey, inF);
		qx_query insQuery ("{\"update\" : \"" + m_valueReturner->getKey()
			+ "\", \"updates\" : [ { \"q\" : " + QString::fromUtf8 (QJsonDocument (filter).toJson()) + ", \"u\" : " + documentToSend + " } ] }");
		/// Make sure user accepts query
		QMessageBox::StandardButton userReply
			= QMessageBox::question (this, "VERIFY QUERY", insQuery.query(), QMessageBox::Apply | QMessageBox::Cancel, QMessageBox::Apply);
		if (userReply != QMessageBox::Apply)
		{
			return;
		}

		QSqlError err = qx::dao::call_query (insQuery);
		if (err.isValid())
		{
			qDebug() << "Error Inserting Data: " << err.text() << err.databaseText() << err.driverText() << err.type();
		}
		qDebug() << insQuery.response();
		QMessageBox::information (this, "Insert Query Response from MONGODB", insQuery.response().toString());
	}
}

void MainWidget::keyPressEvent (QKeyEvent *event)
{
	switch (event->key())
	{
	case Qt::Key_F5:
	{
		restart = true;
		close();
	}
	break;
	case Qt::Key_F3:
	{
		on_pB_SearchFunction_clicked();
	}
	break;
	case Qt::Key_F6:
	{
		on_pB_CreateForm_clicked();
	}
	break;
	case Qt::Key_F7:
	{
		on_pB_Insert_clicked();
	}
	break;
	case Qt::Key_F8:
	{
		on_pB_Update_clicked();
	}
	break;
	default:
		QWidget::keyPressEvent (event);
	}
}
