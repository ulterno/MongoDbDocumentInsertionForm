#ifndef OBJECTFORMMAKER_H
#define OBJECTFORMMAKER_H

#include <QWidget>
#include "common.h"
#include "DocumentIdInserter.h"
#include "ObjectIdInserter.h"
#include "ListFormMaker.h"
#include "StringInserter.h"
#include "IntegerInserter.h"
#include "BooleanInserter.h"
#include "DoubleInserter.h"
#include "DateInserter.h"
#include "TimeInserter.h"
#include "DateTimeInserter.h"
#include "UnRegisteredItemInserter.h"

class ListFormMaker;

namespace Ui
{
class ObjectFormMaker;
}

class ObjectFormMaker : public QWidget, public DataItem
{
	Q_OBJECT

public:
	explicit ObjectFormMaker (const QString& className, const QString& keyName = QString(),
		const DataItem::WidgetConfiguration& config = DataItem::WidgetConfiguration{}, QWidget* parent = nullptr);
	virtual ~ObjectFormMaker();

	void addTypeInserter (qx::IxDataMember* dataMember);
	bool addObjectInserter (const QString& typeName, const QString& keyName);
	bool addListInserter (QString typeName, const QString& keyName);
	bool addUnregisteredItemInserter (const QString& keyName, const QJsonValue& data);

	QJsonValue getValueData () override;
	bool setValueData (const QJsonValue& data) override;

	void setValidators (const QVariantHash& newValidators) override;

	void runValidator (const QVariantHash& validatorData) override;

signals:
	void removeMeFromList ();

	void validatorSignal (QVariantHash info) override;
	void validatorChanged (const QVariantHash& changedValue) override;

private slots:
	void on_pB_Validator_clicked();

private:
	Ui::ObjectFormMaker* ui;
	QList<DataItem*> m_ValueReturners;
	QHash<QString, DataItem*> hash_ValueReturners;
	QString m_keyName;

	QJsonObject m_CachedDataObjects;

	void resolveClassData (const qx::IxClass* clInfo);
	void addIdInserter (qx::IxDataMember* idMember);
	void hashAllValueReturners ();

	template <class T> void registerDataItem (T* item);

	void handleMemberValidators (const QVariantHash& info);

	DataItem::WidgetConfiguration m_config;

	bool checkCacheEqualOtherDoc (const QString& collectionName, const QJsonObject& projection, QJsonArray& returnDataArray);
	QSqlError runDbQueryEqualOtherDoc (const QString& collectionName, const QJsonObject& projection, QJsonArray& returnDataArray);
};

#endif		  // OBJECTFORMMAKER_H
