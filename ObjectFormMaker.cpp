#include "ObjectFormMaker.h"
#include "ui_ObjectFormMaker.h"
#include <QPushButton>
#include <QTimer>
#include "Elements/ValidatorDialogue.h"

ObjectFormMaker::ObjectFormMaker (const QString &className, const QString &keyName, const WidgetConfiguration &config, QWidget *parent)
	: QWidget (parent), DataItem (keyName), ui (new Ui::ObjectFormMaker), m_keyName (keyName), m_config (config)
{
	ui->setupUi (this);
	ui->label_KeyName->setText (keyName);
	connect (ui->pB_Remove, &QPushButton::clicked, this, &ObjectFormMaker::removeMeFromList);
	qx::IxClass *classInfo = qx::QxClassX::getClass (className);
	resolveClassData (classInfo);
	ui->pB_Remove->setEnabled (config.removable);
	ui->pB_Remove->setVisible (config.removable);
	ui->cB_Enable->setChecked (!config.disableByDefault);
	if (!config.disableByDefault)
	{
		connect (ui->cB_Enable, &QCheckBox::toggled, this,
			[this] (bool checked)
			{
			if (checked)
			{
				ui->cB_Enable->setText ("");
				ui->cB_Enable->setStyleSheet ("");
				ui->cB_Enable->setMaximumWidth (32);
			}
			else if (ui->cB_Enable->isEnabled())
			{
				ui->cB_Enable->setText ("THIS KEY WILL BE DELETED ! (If you see red)");
				ui->cB_Enable->setStyleSheet ("*:enabled{ color : Red ; }");
				ui->cB_Enable->setMaximumWidth (400);
			}
		});
	}
	setStyleSheet ("QCheckBox:indicator:enabled { width: 30px; height: 30px; } QCheckBox:indicator:unchecked { background-color: rgb(30, 30, 30); } "
				   ":indicator:enabled:checked { background-color: rgb(90, 255, 219); }");
}

ObjectFormMaker::~ObjectFormMaker() { delete ui; }

void ObjectFormMaker::addTypeInserter (qx::IxDataMember *dataMember)
{
	DataItem::WidgetConfiguration innerObjConfig{};
	innerObjConfig.disableByDefault = m_config.object_disableAllTopLevelElements;

	QString typeName = dataMember->getType();
	typeName.remove (' ');
	if (typeName == "QString")
	{
		auto cx = new StringInserter (dataMember->getKey(), innerObjConfig);
		registerDataItem (cx);
		ui->vL_AllFormInfo->addWidget (cx);
	}
	else if (typeName == "double" || typeName == "float")
	{
		auto cx = new DoubleInserter (dataMember->getKey(), innerObjConfig);
		registerDataItem (cx);
		ui->vL_AllFormInfo->addWidget (cx);
	}
	else if (typeName == "int" || typeName == "long" || typeName == "long long")
	{
		auto cx = new IntegerInserter (dataMember->getKey(), innerObjConfig);
		registerDataItem (cx);
		ui->vL_AllFormInfo->addWidget (cx);
	}
	else if (typeName == "bool")
	{
		auto cx = new BooleanInserter (dataMember->getKey(), innerObjConfig);
		registerDataItem (cx);
		ui->vL_AllFormInfo->addWidget (cx);
	}
	else
	{
		if (typeName == "QStringList")
		{
			typeName = "QList<QString>";
		}
		if (typeName.left (6) == "QList<")
		{
			// List
			qDebug() << "List" << typeName;
			if (!addListInserter (typeName, dataMember->getKey()))
			{
				qDebug() << "Unresolved" << typeName;
			}
		}
		else if (typeName.left (15) == "QSharedPointer<")
		{
			// Object
			qDebug() << "Pointer" << typeName;
			if (dataMember->getSqlRelation())
			{
				// Do something for relation
			}
			else if (addObjectInserter (typeName.mid (15, typeName.length() - 1 - 15), dataMember->getKey()))
			{
				qDebug() << "Unresolved" << typeName;
			}
		}
		else if (typeName == "QDateTime")
		{
			// DateTime
			qDebug() << "DateTime" << typeName;
			auto cx = new DateTimeInserter (dataMember->getKey(), innerObjConfig);
			registerDataItem (cx);
			ui->vL_AllFormInfo->addWidget (cx);
		}
		else if (typeName == "QDate")
		{
			// Date
			qDebug() << "Date" << typeName;
			auto cx = new DateInserter (dataMember->getKey(), innerObjConfig);
			registerDataItem (cx);
			ui->vL_AllFormInfo->addWidget (cx);
		}
		else if (typeName == "QTime")
		{
			// Time
			qDebug() << "Time" << typeName;
			auto cx = new TimeInserter (dataMember->getKey(), innerObjConfig);
			registerDataItem (cx);
			ui->vL_AllFormInfo->addWidget (cx);
		}
		else if (typeName == "ObjectIdType" || typeName == def_ObjectIdType)
		{
			// ObjectIdType
			qDebug() << "ObjectIdType" << typeName;
			auto cx = new ObjectIdInserter (dataMember->getKey(), innerObjConfig);
			registerDataItem (cx);
			ui->vL_AllFormInfo->addWidget (cx);
		}
		else if (addObjectInserter (typeName, dataMember->getKey()))
		{
		}
		else
		{
			qDebug() << "Unresolved" << typeName;
		}
	}
}

bool ObjectFormMaker::addObjectInserter (const QString &typeName, const QString &keyName)
{
	DataItem::WidgetConfiguration innerObjConfig{};
	innerObjConfig.disableByDefault = m_config.object_disableAllTopLevelElements;
	//	qDebug () << "ObjectFormMaker::addObjectInserter(const QString &" << typeName << ", const QString &" << keyName << ")";
	if (qx::QxClassX::getClass (typeName))
	{
		qDebug() << "Inner Object: " << typeName;
		auto cx = new ObjectFormMaker (typeName, keyName, innerObjConfig);
		registerDataItem (cx);
		ui->vL_AllFormInfo->addWidget (cx);
		return true;
	}
	return false;
}

bool ObjectFormMaker::addListInserter (QString typeName, const QString &keyName)
{
	DataItem::WidgetConfiguration innerObjConfig{};
	innerObjConfig.disableByDefault = m_config.object_disableAllTopLevelElements;

	QList<QPair<QString, QString>> typesThatFormLists;
	typesThatFormLists.append (qMakePair<QString, QString> ("QList<", ">"));
	typesThatFormLists.append (qMakePair<QString, QString> ("QVector<", ">"));
	typesThatFormLists.append (qMakePair<QString, QString> ("std::vector<", ">"));
	typesThatFormLists.append (qMakePair<QString, QString> ("QJsonArray<", ">"));
	bool detected = false;
	for (auto typeSpec : typesThatFormLists)
	{
		if ((typeName.left (typeSpec.first.length()) == typeSpec.first) && (typeName.right (typeSpec.second.length()) == typeSpec.second))
		{
			typeName = typeName.mid (typeSpec.first.length(), typeName.length() - typeSpec.second.length() - typeSpec.first.length());
			detected = true;
			break;
		}
	}
	if (detected)
	{
		auto cx = new ListFormMaker (typeName, keyName, innerObjConfig);
		registerDataItem (cx);
		ui->vL_AllFormInfo->addWidget (cx);
		return true;
	}
	return false;
}

bool ObjectFormMaker::addUnregisteredItemInserter (const QString &keyName, const QJsonValue &data)
{
	DataItem::WidgetConfiguration innerObjConfig{};
	innerObjConfig.disableByDefault = m_config.object_disableAllTopLevelElements;

	auto cx = new UnRegisteredItemInserter (keyName, innerObjConfig);
	registerDataItem (cx);
	ui->vL_AllFormInfo->addWidget (cx);
	return cx->setValueData (data);
}

QJsonValue ObjectFormMaker::getValueData()
{
	if (!ui->gB_AllStuff->isEnabled())
	{
		return QJsonValue (QJsonValue::Null);
	}
	QJsonObject retObj;
	for (auto stuff : m_ValueReturners)
	{
		retObj.insert (stuff->getKey(), stuff->getValueData());
	}
	return retObj;
}

bool ObjectFormMaker::setValueData (const QJsonValue &data)
{
	if (!data.isObject())
	{
		b_failedSettingData = true;
		return false;
	}
	b_failedSettingData = false;
	const QJsonObject dObj = data.toObject();
	for (auto aKey : dObj.keys())
	{
		if (hash_ValueReturners.find (aKey) == hash_ValueReturners.end())
		{
			qDebug() << "Object: " << m_keyName << " for Key: " << aKey << " No element exists.";
			if (!addUnregisteredItemInserter (aKey, dObj[aKey]))
			{
				b_failedSettingData = true;
			}
		}
		else if (!hash_ValueReturners[aKey]->setValueData (dObj[aKey]))
		{
			qDebug() << "Object: " << m_keyName << " for Key: " << aKey << " Failed to set Value" << dObj[aKey];
			/// In case value is registered, but of the wrong type, causing translation to fail,
			/// It is upto the user to make sure only one of the 2 is selected, lest causing runtime defined behaviour.
			if (!addUnregisteredItemInserter (aKey, dObj[aKey]))
			{
				b_failedSettingData = true;
			}
		}
	}
	return !b_failedSettingData;
}

void ObjectFormMaker::setValidators (const QVariantHash &newValidators)
{
	for (QString key : newValidators.keys())
	{
		if (key == "$$this")
		{
			continue;
		}
		if (newValidators[key].toJsonObject().isEmpty())
		{
			qDebug() << "Maybe Wrong Validator : " << ui->label_KeyName->text() << key << newValidators[key];
			continue;
		}
		QJsonObject valObj = newValidators[key].toJsonObject();
		if (valObj.contains ("type"))
		{
			if (hash_ValueReturners.contains (key))
			{
				QVariantHash innerValidator;
				innerValidator.insert ("$$this", valObj);
				QTimer::singleShot (0, this, [this, key, innerValidator] () { hash_ValueReturners[key]->setValidators (innerValidator); });
			}
		}
		else
		{
			qDebug() << "Validator Has No type : " << ui->label_KeyName->text() << key << newValidators[key];
		}
	}
	return DataItem::setValidators (newValidators);
}

void ObjectFormMaker::runValidator (const QVariantHash &validatorData) {}

void ObjectFormMaker::resolveClassData (const qx::IxClass *clInfo)
{
	if (clInfo)
	{
		auto idMember = clInfo->getId();
		if (idMember)
		{
			qDebug() << "ID: " << idMember->getType() << idMember->getKey() << idMember->getName();
			addIdInserter (idMember);
		}
		auto dataMemberList = clInfo->getDataMemberX();

		/// Ignored data members
		QStringList ignoredDataMembersList{};
		QVariant ignoredDataMembersVar = clInfo->getPropertyBag ("ignore data members");
		if (ignoredDataMembersVar.canConvert (QMetaType::QStringList))
		{
			ignoredDataMembersList = ignoredDataMembersVar.toStringList();
		}

		/// Insert Data Members
		long nOfDMs = dataMemberList->count();
		qDebug() << clInfo->getName() << ": No. of data members: " << nOfDMs;
		for (long iter = 0; iter < nOfDMs; iter++)
		{
			auto dm = dataMemberList->get (iter);
			if (ignoredDataMembersList.contains (dm->getKey()))
			{
				qDebug() << "Ignoring Data Member: " << dm->getType() << dm->getKey() << dm->getName();
				continue;
			}
			if (dm == idMember)
			{
				continue;
			}
			qDebug() << "Data Member: " << dm->getType() << dm->getKey() << dm->getName();
			addTypeInserter (dm);
		}

		/// Recurse for all base classes
		qx::IxClass *bclInfo = clInfo->getBaseClass();
		resolveClassData (bclInfo);

		/// Can't run virtual functions in Constructor!? 😢
		QTimer::singleShot (0, this,
			[this, clInfo] ()
			{
			/// Validators
			QVariant potential_validators = clInfo->getPropertyBag ("validators");
			if (potential_validators.isValid())
			{
				QVariantHash validators = potential_validators.toHash();
				setValidators (validators);
			}
		});
	}
}

void ObjectFormMaker::addIdInserter (qx::IxDataMember *idMember)
{
	DataItem::WidgetConfiguration docIdConfig{};
	docIdConfig.enableDocumentId = m_config.enableDocumentId;
	auto cx = new DocumentIdInserter (idMember->getKey(), docIdConfig);
	registerDataItem (cx);
	ui->vL_AllFormInfo->addWidget (cx);
}

void ObjectFormMaker::hashAllValueReturners()
{
	for (DataItem *vr : m_ValueReturners)
	{
		hash_ValueReturners.insert (vr->getKey(), vr);
	}
}

void ObjectFormMaker::handleMemberValidators (const QVariantHash &info)
{
	if (info.contains("fill current") && info.contains ("$$me"))
	{
		if (!hash_ValueReturners.contains(info["$$me"].toString()))
		{
			qDebug () << "DataItem not found: " << info["$$me"].toString();
		}
		hash_ValueReturners[info["$$me"].toString()]->setValueData(info["fill current"].toJsonValue());
	}
	if (info.contains("fill related") && info.contains ("$$me"))
	{
		QVariantHash validators = getValidators();
		if (validators.contains(info["$$me"].toString()))
		{
			QJsonObject valEntry = validators[info["$$me"].toString()].toJsonObject();
			if (valEntry["equal other doc"].isObject())
			{
				QJsonObject myEodEntry = valEntry["equal other doc"].toObject();
				if (myEodEntry["local field for relation"].toString() != info["$$me"].toString())
				{
					if (hash_ValueReturners.contains(myEodEntry["local field for relation"].toString()))
					{
						hash_ValueReturners[myEodEntry["local field for relation"].toString()]->setValueData(info["fill related"].toJsonValue());
					}
					else
					{
						qDebug () << "DataItem not found: " << myEodEntry["local field for relation"].toString();
					}
				}
				else
				{
					// Fail
				}
			}
			else
			{
				// Fail
			}
		}
		else
		{
			// Fail
		}
	}
	if (info.contains("fill all"))
	{
		// Resolve remote-local connections in all validators
		///
		/// \brief remote_local QHash<QString remote key name, QString local key name>
		///
		QHash<QString, QString> remote_local;
		{
			QVariantHash validators = getValidators();
			for (QString key : validators.keys())
			{
				QJsonObject valEntry = validators[key].toJsonObject();
				if ((!valEntry.contains("equal other doc")) || (!hash_ValueReturners.contains(key)))
				{
					continue;
				}
				remote_local.insert(valEntry["equal other doc"].toObject()["remote field"].toString(), key);
			}
		}
		// Add data to whichever is  available
		// ToDo: Resolve inner objects
		QJsonObject obj = info["fill all"].toJsonObject();
		if (!obj.isEmpty())
		{
			for (QString key : obj.keys())
			{
				if (hash_ValueReturners.contains(remote_local[key]))
				{
					hash_ValueReturners[remote_local[key]]->setValueData(obj[key]);
				}
			}
		}
	}
	if (info.contains ("equal other doc") && info.contains ("$$me"))
	{
		QJsonObject eodObj = info["equal other doc"].toJsonObject();
		// ToDo: add checking at each step to prevent crashes
		QString me = info["$$me"].toString();
		QString related_field = eodObj["remote field for relation"].toString();
		QString remote_field = eodObj["remote field"].toString();
		QJsonArray propag_arr = eodObj["propagate to fields"].toArray();
		QJsonObject projection;
		projection.insert (remote_field, 1);
		if (!related_field.isEmpty())
		{
			projection.insert (related_field, 1);
		}
		QVariantHash validators = getValidators();
		validators.remove (me);
		for (auto proIter : propag_arr)
		{
			QString local_field = proIter.toString();
			if (validators[local_field].isValid())
			{
				QJsonObject inEod = validators[local_field].toJsonObject()["equal other doc"].toObject();
				projection.insert (inEod["remote field"].toString(), 1);
				QString related_field = inEod["remote field for relation"].toString();
				if (!related_field.isEmpty())
				{
					projection.insert (related_field, 1);
				}
			}
		}
		QJsonObject eodRetObj;
		QVariantHash retVar;
		QJsonArray returnDataArray;
		if (eodObj["collection"].isString())
		{
			QString collection_name = eodObj["collection"].toString();
			// Check first if cached data is available. Cache will have to be made in each object.
			QJsonArray tempReturnDataArray;
			QSqlError err;
			if (!info["$$refresh"].toBool() && checkCacheEqualOtherDoc (collection_name, projection, tempReturnDataArray))
			{
				QJsonObject eodCacheObj;
				eodCacheObj.insert ("data", tempReturnDataArray);
				returnDataArray = tempReturnDataArray;
			}
			else
			{
				err = runDbQueryEqualOtherDoc (collection_name, projection, tempReturnDataArray);

				if (err.isValid())
				{
					qDebug() << getKey() + " -> ObjectFormMaker::handleMemberValidators -> " + me << err.text() << err.databaseText() << err.driverText();
				}
				else
				{
					QJsonObject eodCacheObject;
					eodCacheObject.insert ("data", tempReturnDataArray);
					eodCacheObject.insert ("projection", projection);
					returnDataArray = tempReturnDataArray;
					m_CachedDataObjects.insert (collection_name, eodCacheObject);
				}
			}
		}
		else if (eodObj["collection"].isArray())
		{
			for (auto arrIter : eodObj["collection"].toArray())
			{
				QString collection_name = arrIter.toString();
				// Check first if cached data is available. Cache will have to be made in each object.
				QJsonArray tempReturnDataArray;
				QSqlError err;
				if (!info["$$refresh"].toBool() && checkCacheEqualOtherDoc (collection_name, projection, tempReturnDataArray))
				{
					QJsonObject eodCacheObj;
					eodCacheObj.insert ("data", tempReturnDataArray);
					for (auto x : tempReturnDataArray)
					{
						returnDataArray.append(x);
					}
				}
				else
				{
					err = runDbQueryEqualOtherDoc (collection_name, projection, tempReturnDataArray);

					if (err.isValid())
					{
						qDebug() << getKey() + " -> ObjectFormMaker::handleMemberValidators -> " + me << err.text() << err.databaseText() << err.driverText();
					}
					else
					{
						QJsonObject eodCacheObject;
						eodCacheObject.insert ("data", tempReturnDataArray);
						eodCacheObject.insert ("projection", projection);
						for (auto x : tempReturnDataArray)
						{
							returnDataArray.append(x);
						}
						m_CachedDataObjects.insert (collection_name, eodCacheObject);
					}
				}
			}
		}
		eodRetObj.insert("data", returnDataArray);
		eodRetObj.insert("projection", projection);
		retVar.insert ("equal other doc", eodRetObj);
		hash_ValueReturners[me]->runValidator (retVar);
	}
}

bool ObjectFormMaker::checkCacheEqualOtherDoc (const QString &collectionName, const QJsonObject &projection, QJsonArray &returnDataArray)
{
	if (!m_CachedDataObjects.contains (collectionName))
	{
		return false;
	}
	QJsonObject eodRetObj = m_CachedDataObjects[collectionName].toObject();
	// ToDo: Optionally, check how stale cache is. Should not be required since, the form is supposed to be reset regularly
	// Check for availability of all projection items
	if (!eodRetObj.contains ("projection"))
	{
		return false;
	}
	QJsonObject erObj = eodRetObj["projection"].toObject();
	for (QString key : projection.keys())
	{
		if (!erObj.contains (key))
		{
			return false;
		}
	}
	returnDataArray = eodRetObj["data"].toArray();
	if (returnDataArray.isEmpty())
	{
		return false;
	}
	return true;
}

QSqlError ObjectFormMaker::runDbQueryEqualOtherDoc (const QString &collectionName, const QJsonObject &projection, QJsonArray &returnDataArray)
{
	qx_query gdQuery (
		"cursor", "{\"find\" : \"" + collectionName + "\", \"projection\" : " + QString::fromUtf8 (QJsonDocument (projection).toJson()) + "}");
	QSqlError err = qx::dao::call_query (gdQuery);
	returnDataArray = QJsonDocument::fromJson (gdQuery.response().toString().toUtf8()).array();
	return err;
}

template <class T> void ObjectFormMaker::registerDataItem (T *item)
{
	m_ValueReturners << item;
	hash_ValueReturners.insert (item->getKey(), item);
	connect (item, &T::validatorSignal, this, &ObjectFormMaker::handleMemberValidators);
}

void ObjectFormMaker::on_pB_Validator_clicked()
{
	QVariantHash relevantData;
	if (!m_CachedDataObjects.isEmpty())
	{
		relevantData.insert("doc", m_CachedDataObjects);
	}
	relevantData.insert("$$me", getKey());
	relevantData.insert("current value", getValueData());
	QVariantHash returnTypeData;
	returnTypeData.insert("type", "string");
	QVariantHash userRetVal = ValidatorDialogue::showValidatorWidget(getValidators(), relevantData, returnTypeData);
	userRetVal.insert("$$me", getKey());
	emit validatorSignal(userRetVal);
}

