#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QPointer>
#include <ObjectFormMaker.h>
#include <ListFormMaker.h>
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWidget;
}
QT_END_NAMESPACE

class MainWidget : public QWidget
{
	Q_OBJECT

public:
	MainWidget (QWidget *parent = nullptr);
	virtual ~MainWidget();

	bool restart{};

private slots:
	void on_pB_CreateForm_clicked ();

	void on_pB_Insert_clicked ();

	void on_pB_SearchFunction_clicked ();

	void on_pB_Update_clicked ();

protected:
	virtual void keyPressEvent (QKeyEvent *event);

private:
	Ui::MainWidget *ui;
	DataItem *m_valueReturner = nullptr;
	DataItem::WidgetConfiguration m_config;
};
#endif		  // MAINWIDGET_H
