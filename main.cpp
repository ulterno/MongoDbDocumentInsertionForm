#include "MainWidget.h"

#include <QApplication>

int main (int argc, char *argv[])
{
	QApplication a (argc, argv);
	int retVal;
	bool restart{};
	MainWidget *w;
	do
	{
		w = new MainWidget;
		w->showMaximized();
		retVal = a.exec();
		restart = w->restart;
		delete w;
	}
	while (restart);
	return retVal;
}
