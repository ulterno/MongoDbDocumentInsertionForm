#ifndef DOCUMENTIDINSERTER_H
#define DOCUMENTIDINSERTER_H

#include <QWidget>
#include "DataItem.h"

namespace Ui
{
class DocumentIdInserter;
}

class DocumentIdInserter : public QWidget, public DataItem
{
	Q_OBJECT

public:
	explicit DocumentIdInserter (const QString &keyName, const DataItem::WidgetConfiguration &config, QWidget *parent = nullptr);
	virtual ~DocumentIdInserter();

	QJsonValue getValueData () override;
	bool setValueData (const QJsonValue &data) override;

	void runValidator (const QVariantHash &validatorData) override;

signals:
	void validatorSignal (QVariantHash info) override;
	void validatorChanged (const QVariantHash &changedValue) override;

private:
	Ui::DocumentIdInserter *ui;
	DataItem::WidgetConfiguration m_config;
};

#endif		  // DOCUMENTIDINSERTER_H
