#include "ListFormMaker.h"
#include "ObjectFormMaker.h"
#include "ui_ListFormMaker.h"
#include "Elements/ValidatorDialogue.h"

ListFormMaker::ListFormMaker (const QString &typeName, const QString &keyName, const WidgetConfiguration &config, QWidget *parent)
	: QWidget (parent), DataItem (keyName), ui (new Ui::ListFormMaker), m_typeName (typeName), m_keyName (keyName), m_config (config)
{
	ui->setupUi (this);
	ui->label_Title->setText (keyName);
	inObjsLayout = new QVBoxLayout;
	ui->scrollAreaWidgetContents->setLayout (inObjsLayout);
	ui->cB_Enable->setChecked (!config.disableByDefault);
	if (!config.disableByDefault)
	{
		connect (ui->cB_Enable, &QCheckBox::toggled, this,
			[this] (bool checked)
			{
			if (checked)
			{
				ui->cB_Enable->setText ("");
				ui->cB_Enable->setStyleSheet ("");
				ui->cB_Enable->setMaximumWidth (32);
			}
			else if (ui->cB_Enable->isEnabled())
			{
				ui->cB_Enable->setText ("THIS KEY WILL BE DELETED ! (If you see red)");
				ui->cB_Enable->setStyleSheet ("*:enabled{ color : Red ; }");
				ui->cB_Enable->setMaximumWidth (400);
			}
		});
	}
}

ListFormMaker::~ListFormMaker() { delete ui; }

DataItem *ListFormMaker::addTypeInserter()
{
	DataItem::WidgetConfiguration innerObjConfig{};
	innerObjConfig.removable = true;

	DataItem *xx = nullptr;
	;
	m_typeName.remove (' ');
	if (m_typeName == "QString")
	{
		auto cx = new StringInserter (m_typeName, innerObjConfig);
		m_ValueReturners << cx;
		inObjsLayout->addWidget (cx);
		addCurrentValidators (cx);
		return cx;
	}
	else if (m_typeName == "double" || m_typeName == "float")
	{
		auto cx = new DoubleInserter (m_typeName, innerObjConfig);
		m_ValueReturners << cx;
		inObjsLayout->addWidget (cx);
		addCurrentValidators (cx);
		return cx;
	}
	else if (m_typeName == "int" || m_typeName == "long" || m_typeName == "long long")
	{
		auto cx = new IntegerInserter (m_typeName, innerObjConfig);
		m_ValueReturners << cx;
		inObjsLayout->addWidget (cx);
		addCurrentValidators (cx);
		return cx;
	}
	else if (m_typeName == "bool")
	{
		auto cx = new BooleanInserter (m_typeName, innerObjConfig);
		m_ValueReturners << cx;
		inObjsLayout->addWidget (cx);
		addCurrentValidators (cx);
		return cx;
	}
	else
	{
		if (m_typeName == "QStringList")
		{
			m_typeName = "QList<QString>";
		}
		if (m_typeName.left (6) == "QList<")
		{
			// List
			qDebug() << "List" << m_typeName;
			if (!addListInserter (m_typeName, m_keyName, &xx))
			{
				qDebug() << "Unresolved" << m_typeName;
			}
			return xx;
		}
		else if (m_typeName.left (15) == "QSharedPointer<")
		{
			// Object
			qDebug() << "Pointer" << m_typeName;
			// Nothing for relation in this case
			if (addObjectInserter (m_typeName.mid (15, m_typeName.length() - 1 - 15), m_typeName, &xx))
			{
				qDebug() << "Unresolved" << m_typeName;
				return xx;
			}
		}
		else if (m_typeName == "QDateTime")
		{
			// DateTime
			qDebug() << "DateTime" << m_typeName;
			auto cx = new DateTimeInserter (m_typeName, innerObjConfig);
			m_ValueReturners << cx;
			inObjsLayout->addWidget (cx);
			addCurrentValidators (cx);
			return cx;
		}
		else if (m_typeName == "QDate")
		{
			// Date
			qDebug() << "Date" << m_typeName;
			auto cx = new DateInserter (m_typeName, innerObjConfig);
			m_ValueReturners << cx;
			inObjsLayout->addWidget (cx);
			addCurrentValidators (cx);
			return cx;
		}
		else if (m_typeName == "QTime")
		{
			// Time
			qDebug() << "Time" << m_typeName;
			auto cx = new TimeInserter (m_typeName, innerObjConfig);
			m_ValueReturners << cx;
			inObjsLayout->addWidget (cx);
			addCurrentValidators (cx);
			return cx;
		}
		else if (m_typeName == "ObjectIdType" || m_typeName == def_ObjectIdType)
		{
			// ObjectIdType
			qDebug() << "ObjectIdType" << m_typeName;
			auto cx = new ObjectIdInserter (m_typeName, innerObjConfig);
			m_ValueReturners << cx;
			inObjsLayout->addWidget (cx);
			addCurrentValidators (cx);
			return cx;
		}
		else if (addObjectInserter (m_typeName, m_typeName, &xx))
		{
			return xx;
		}
		else
		{
			qDebug() << "Unresolved" << m_typeName;
		}
	}
	return xx;
}

bool ListFormMaker::addTypeInserter (const QJsonValue &data)
{
	DataItem *item = addTypeInserter();
	return item->setValueData (data);
}

bool ListFormMaker::addObjectInserter (const QString &typeName, const QString &keyName, DataItem **xx)
{
	//	qDebug () << "ObjectFormMaker::addObjectInserter(const QString &" << typeName << ", const QString &" << keyName << ")";
	if (qx::QxClassX::getClass (typeName))
	{
		qDebug() << "Inner Object: " << typeName;
		DataItem::WidgetConfiguration objConfig{};
		objConfig.removable = true;
		auto cx = new ObjectFormMaker (typeName, keyName, objConfig);
		m_ValueReturners << cx;
		addCurrentValidators (cx);
		allObjects << cx;
		*xx = cx;
		inObjsLayout->addWidget (cx);
		connect (cx, &ObjectFormMaker::removeMeFromList, inObjsLayout,
			[this, cx] ()
			{
			cx->setEnabled (false);
			m_ValueReturners.removeAll (cx);
		});
		if (allObjects.length() > 1)
		{
			ui->scrollArea->setMinimumHeight (allObjects.at (0)->height() * 1.5);
		}
		else if (allObjects.length() > 0)
		{
			ui->scrollArea->setMinimumHeight (allObjects.at (0)->height() * 0.75);
		}
		return true;
	}
	return false;
}

bool ListFormMaker::addListInserter (QString typeName, const QString &keyName, DataItem **xx)
{
	DataItem::WidgetConfiguration innerObjConfig{};
	innerObjConfig.removable = true;

	QList<QPair<QString, QString>> typesThatFormLists;
	typesThatFormLists.append (qMakePair<QString, QString> ("QList<", ">"));
	typesThatFormLists.append (qMakePair<QString, QString> ("QVector<", ">"));
	typesThatFormLists.append (qMakePair<QString, QString> ("std::vector<", ">"));
	typesThatFormLists.append (qMakePair<QString, QString> ("QJsonArray<", ">"));
	bool detected = false;
	for (auto typeSpec : typesThatFormLists)
	{
		if ((typeName.left (typeSpec.first.length()) == typeSpec.first) && (typeName.right (typeSpec.second.length()) == typeSpec.second))
		{
			typeName = typeName.mid (typeSpec.first.length(), typeName.length() - typeSpec.second.length() - typeSpec.first.length());
			detected = true;
			break;
		}
	}
	if (detected)
	{
		auto cx = new ListFormMaker (typeName, keyName, innerObjConfig);
		m_ValueReturners << cx;
		addCurrentValidators (cx);
		*xx = cx;
		inObjsLayout->addWidget (cx);
		return true;
	}
	return false;
}

bool ListFormMaker::addUnregisteredItemInserter (const QString &keyName, const QJsonValue &data)
{
	DataItem::WidgetConfiguration innerObjConfig{};
	innerObjConfig.disableByDefault = m_config.object_disableAllTopLevelElements;

	auto cx = new UnRegisteredItemInserter (keyName, innerObjConfig);
	m_ValueReturners << cx;
	addCurrentValidators (cx);
	inObjsLayout->addWidget (cx);
	return cx->setValueData (data);
}

QJsonValue ListFormMaker::getValueData()
{
	if (!ui->scrollArea->isEnabled())
	{
		return QJsonValue (QJsonValue::Null);
	}
	QJsonArray retArr;
	for (auto stuff : m_ValueReturners)
	{
		retArr.append (stuff->getValueData());
	}
	return retArr;
}

bool ListFormMaker::setValueData (const QJsonValue &data)
{
	if (!data.isArray())
	{
		b_failedSettingData = true;
		return false;
	}
	b_failedSettingData = false;
	const QJsonArray dArr = data.toArray();
	for (auto entry : dArr)
	{
		if (!addTypeInserter (entry))
		{
			qDebug() << "Array: " << m_typeName << m_keyName << " for entry: " << entry << " failed to set value.";
			if (!addUnregisteredItemInserter (m_typeName, entry))
			{
				b_failedSettingData = true;
			}
		}
	}
	return !b_failedSettingData;
}

void ListFormMaker::setValidators (const QVariantHash &newValidators)
{
	if (newValidators.contains ("$$this"))
	{
		QVariantHash innerValidator;
		innerValidator.insert ("$$listThis", newValidators["$$this"]);
		for (auto re : m_ValueReturners)
		{
			re->setValidators (innerValidator);
		}
	}
	return DataItem::setValidators (newValidators);
}

void ListFormMaker::runValidator (const QVariantHash &validatorData) {}

void ListFormMaker::on_pB_AddObj_clicked() { addTypeInserter(); }

void ListFormMaker::addCurrentValidators (DataItem *innerObj)
{
	const QVariantHash validators = getValidators();
	if (validators.contains ("$$this"))
	{
		QVariantHash innerValidator;
		innerValidator.insert ("$$listThis", validators["$$this"]);
		innerObj->setValidators (innerValidator);
	}
}

void ListFormMaker::on_pB_Validator_clicked()
{
	QVariantHash relevantData;
	relevantData.insert("$$me", getKey());
	relevantData.insert("current value", getValueData());
	QVariantHash returnTypeData;
	returnTypeData.insert("type", "string");
	QVariantHash userRetVal = ValidatorDialogue::showValidatorWidget(getValidators(), relevantData, returnTypeData);
	userRetVal.insert("$$me", getKey());
	emit validatorSignal(userRetVal);
}

