#ifndef LISTFORMMAKER_H
#define LISTFORMMAKER_H

#include <QWidget>
#include "common.h"
#include <QVBoxLayout>
#include "DataItem.h"

class ObjectFormMaker;

namespace Ui
{
class ListFormMaker;
}

class ListFormMaker : public QWidget, public DataItem
{
	Q_OBJECT

public:
	explicit ListFormMaker (const QString& typeName, const QString& keyName, const DataItem::WidgetConfiguration& config, QWidget* parent = nullptr);
	virtual ~ListFormMaker();

	// ToDo: This is being a duplicate. Fix it
	DataItem* addTypeInserter ();
	bool addTypeInserter (const QJsonValue& data);
	bool addObjectInserter (const QString& typeName, const QString& keyName, DataItem** xx);
	bool addListInserter (QString typeName, const QString& keyName, DataItem** xx);
	bool addUnregisteredItemInserter (const QString& keyName, const QJsonValue& data);

	QJsonValue getValueData () override;
	bool setValueData (const QJsonValue& data) override;

	void setValidators (const QVariantHash& newValidators) override;

	void runValidator (const QVariantHash& validatorData) override;

signals:
	void validatorSignal (QVariantHash info) override;
	void validatorChanged (const QVariantHash& changedValue) override;

private slots:
	void on_pB_AddObj_clicked ();

	void on_pB_Validator_clicked();

private:
	Ui::ListFormMaker* ui;
	QVBoxLayout* inObjsLayout;
	QList<ObjectFormMaker*> allObjects;
	QList<DataItem*> m_ValueReturners;
	void addCurrentValidators (DataItem* innerObj);
	QString m_typeName, m_keyName;
	DataItem::WidgetConfiguration m_config;
};

#endif		  // LISTFORMMAKER_H
