#ifndef STRINGINSERTER_H
#define STRINGINSERTER_H

#include <QWidget>
#include "DataItem.h"

namespace Ui
{
class StringInserter;
}

class StringInserter : public QWidget, public DataItem
{
	Q_OBJECT

public:
	explicit StringInserter (const QString &keyName, const DataItem::WidgetConfiguration &config, QWidget *parent = nullptr);
	virtual ~StringInserter();

	QJsonValue getValueData () override;
	bool setValueData (const QJsonValue &data) override;

	void runValidator (const QVariantHash &validatorData) override;

signals:
	void validatorSignal (QVariantHash info) override;
	void validatorChanged (const QVariantHash &changedValue) override;

private slots:
	void on_pB_Validator_clicked();

private:
	Ui::StringInserter *ui;
	DataItem::WidgetConfiguration m_config;

	void applyValidators ();
	struct ToDo
	{
		bool validators_done{};
		bool equal_other_doc{};
		QList<QChar> forbidden_characters;
		enum ValidateCase
		{
			VC_No = 0,
			VC_Caps,
			VC_Small
		};
		ValidateCase validate_case{};
		struct Indicate
		{
			bool case_invalid{};
			bool forbidden_characters_invalid{};
			bool equal_other_doc_invalid{};
		} indicate{};
	} todo{};
	bool getCase_invalid() const;
	void setCase_invalid(bool newCase_invalid);
	bool getForbidden_characters_invalid() const;
	void setForbidden_characters_invalid(bool newForbidden_characters_invalid);
	bool getEqual_other_doc_invalid() const;
	void setEqual_other_doc_invalid(bool newEqual_other_doc_invalid);
	QJsonArray m_storedOtherDocsData;		 // To be used for validator widget
	QStringList m_currentCompletionList;
	void scriptControl ();
	void forbiddenCharactersValidatorInstance (const QString &lineEditText);
	void caseValidatorInstance (const QString &lineEditText);
	void equalOtherDocValidatorInstance (const QString &lineEditText);
	void runEqualOtherDoc (const QVariantHash &validatorData);
};

#endif		  // STRINGINSERTER_H
