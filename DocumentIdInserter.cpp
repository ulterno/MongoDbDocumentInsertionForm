#include "DocumentIdInserter.h"
#include "ui_DocumentIdInserter.h"

#include <QJsonObject>

DocumentIdInserter::DocumentIdInserter (const QString &keyName, const WidgetConfiguration &config, QWidget *parent)
	: QWidget (parent), DataItem (keyName), ui (new Ui::DocumentIdInserter), m_config (config)
{
	ui->setupUi (this);
	ui->label_Key->setText (keyName);
	ui->cB_ManualId->setChecked (false);
}

DocumentIdInserter::~DocumentIdInserter() { delete ui; }

QJsonValue DocumentIdInserter::getValueData()
{
	if (ui->lE_ObjectId->isEnabled() || m_config.enableDocumentId)
	{
		QJsonObject objectId;
		objectId.insert ("$oid", ui->lE_ObjectId->text());
		return objectId;
	}
	return QJsonValue (QJsonValue::Null);
}

bool DocumentIdInserter::setValueData (const QJsonValue &data)
{
	bool retVal = false;
	QString store;
	store = data.toString();
	if (store.isEmpty() && data.isObject())
	{
		QJsonObject obj = data.toObject();
		if (obj.contains ("$oid"))
		{
			store = obj.value ("$oid").toString();
			ui->lE_ObjectId->setText (store);
			retVal = true;
		}
	}
	else
	{
		retVal = true;
		ui->lE_ObjectId->setText (store);
	}
	b_failedSettingData = !retVal;
	return retVal;
}

void DocumentIdInserter::runValidator (const QVariantHash &validatorData) {}
