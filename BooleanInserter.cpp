#include "BooleanInserter.h"
#include "ui_BooleanInserter.h"
#include "Elements/ValidatorDialogue.h"

BooleanInserter::BooleanInserter (const QString &keyName, const WidgetConfiguration &config, QWidget *parent)
	: QWidget (parent), DataItem (keyName), ui (new Ui::BooleanInserter), m_config (config)
{
	ui->setupUi (this);
	ui->label_Key->setText (keyName);
	ui->cB_Enable->setChecked (!config.disableByDefault);
	if (!config.disableByDefault)
	{
		connect (ui->cB_Enable, &QCheckBox::toggled, this,
			[this] (bool checked)
			{
			if (checked)
			{
				ui->cB_Enable->setText ("");
				ui->cB_Enable->setStyleSheet ("");
				ui->cB_Enable->setMaximumWidth (32);
			}
			else if (ui->cB_Enable->isEnabled())
			{
				ui->cB_Enable->setText ("THIS KEY WILL BE DELETED ! (If you see red)");
				ui->cB_Enable->setStyleSheet ("*:enabled{ color : Red ; }");
				ui->cB_Enable->setMaximumWidth (400);
			}
		});
	}
	connect (this, &BooleanInserter::validatorChanged, this,
		[this] (const QVariantHash &)
		{
		todo.validators_done = false;
		scriptControl();
	});
}

BooleanInserter::~BooleanInserter() { delete ui; }

QJsonValue BooleanInserter::getValueData()
{
	if (!ui->cB_Value->isEnabled())
	{
		return QJsonValue (QJsonValue::Null);
	}
	return ui->cB_Value->isChecked();
}

bool BooleanInserter::setValueData (const QJsonValue &data)
{
	bool retVal;
	bool store;
	retVal = qx::cvt::detail::QxConvert_FromJson<bool>::fromJson (data, store, QString());
	if (retVal)
	{
		ui->cB_Value->setChecked (store);
	}
	b_failedSettingData = !retVal;
	return retVal;
}

void BooleanInserter::runValidator (const QVariantHash &validatorData) { runEqualOtherDoc (validatorData); }

void BooleanInserter::applyValidators()
{
	QVariantHash validators = getValidators();
	if (validators.isEmpty())
	{
		return;
	}
	QJsonObject valsOfThis = validators["$$this"].toJsonObject();
	if (valsOfThis.isEmpty())
	{
		return;
	}
	if (valsOfThis.contains ("equal other doc"))
	{
		QJsonObject equal_other_doc_options = valsOfThis["equal other doc"].toObject();
		if (equal_other_doc_options.contains ("collection"))
		{
			// Make equal_other_doc options
			// Send document request
			QVariantHash eodRequest;
			eodRequest.insert ("equal other doc", valsOfThis["equal other doc"].toObject());
			eodRequest.insert ("$$me", getKey());
			// Set receiver on
			todo.equal_other_doc = true;
			emit validatorSignal (eodRequest);
		}
		connect (ui->cB_Value, QOverload<bool>::of(&QCheckBox::toggled), this, &BooleanInserter::equalOtherDocValidatorInstance, Qt::UniqueConnection);
	}
	else
	{
		disconnect (ui->cB_Value, QOverload<bool>::of(&QCheckBox::toggled), this, &BooleanInserter::equalOtherDocValidatorInstance);
	}
}

bool BooleanInserter::getCase_invalid() const
{
	return todo.indicate.case_invalid;
}

void BooleanInserter::setCase_invalid(bool newCase_invalid)
{
	ui->w_Indicators->setCapsValid(!newCase_invalid);
	todo.indicate.case_invalid = newCase_invalid;
}

bool BooleanInserter::getForbidden_characters_invalid() const
{
	return todo.indicate.forbidden_characters_invalid;
}

void BooleanInserter::setForbidden_characters_invalid(bool newForbidden_characters_invalid)
{
	ui->w_Indicators->setForbiddenValid(!newForbidden_characters_invalid);
	todo.indicate.forbidden_characters_invalid = newForbidden_characters_invalid;
}

bool BooleanInserter::getEqual_other_doc_invalid() const
{
	return todo.indicate.equal_other_doc_invalid;
}

void BooleanInserter::setEqual_other_doc_invalid(bool newEqual_other_doc_invalid)
{
	ui->w_Indicators->setEqualOtherDocValid(!newEqual_other_doc_invalid);
	todo.indicate.equal_other_doc_invalid = newEqual_other_doc_invalid;
}

void BooleanInserter::scriptControl()
{
	if (todo.validators_done)
	{
		return;
	}
	// Do Validator stuff
	applyValidators();
	todo.validators_done = true;
}

void BooleanInserter::equalOtherDocValidatorInstance(const bool &currentData)
{
	if (m_currentCompletionList.contains (currentData))
	{
		setEqual_other_doc_invalid(false);
		return;
	}
	setEqual_other_doc_invalid(true);
}

void BooleanInserter::runEqualOtherDoc(const QVariantHash &validatorData)
{
	QVariantHash validators = getValidators();
	QJsonObject eodObj = validators["$$this"].toJsonObject()["equal other doc"].toObject();
	if (todo.equal_other_doc && validatorData.contains ("equal other doc"))
	{
		todo.equal_other_doc = false;
		QJsonObject eodReturn = validatorData["equal other doc"].toJsonObject();
		QJsonArray otherDocsData = eodReturn["data"].toArray();
		m_storedOtherDocsData = otherDocsData;
		if (otherDocsData.isEmpty())
		{
			m_currentCompletionList.clear();
			return;
		}
		for (auto objIter : otherDocsData)
		{
			if (!objIter.isObject())
			{
				continue;
			}
			QJsonObject aObj = objIter.toObject();
			if (aObj.contains (eodObj["remote field"].toString()))		  // ToDo: Make logic for cases where fields are not top level
			{
				m_currentCompletionList.append (aObj[eodObj["remote field"].toString()].toBool());
			}
		}
	}
}

void BooleanInserter::on_pB_Validator_clicked()
{
	QVariantHash relevantData;
	if (!m_storedOtherDocsData.isEmpty())
	{
		relevantData.insert("doc", m_storedOtherDocsData);
	}
	relevantData.insert("$$me", getKey());
	relevantData.insert("current value", ui->cB_Value->isChecked());
	QVariantHash returnTypeData;
	returnTypeData.insert("type", "string");
	QVariantHash userRetVal = ValidatorDialogue::showValidatorWidget(getValidators(), relevantData, returnTypeData);
	userRetVal.insert("$$me", getKey());
	emit validatorSignal(userRetVal);
}
