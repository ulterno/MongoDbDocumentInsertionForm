QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    BooleanInserter.cpp \
    DataItem.cpp \
    DateInserter.cpp \
    DateTimeInserter.cpp \
    DocumentIdInserter.cpp \
    DoubleInserter.cpp \
    Elements/ObjectPickerDialog.cpp \
    Elements/ScrollLessEdits.cpp \
    Elements/ValidatorDialogue.cpp \
    Elements/ValidatorIndicatorsSet.cpp \
    IntegerInserter.cpp \
    ListFormMaker.cpp \
    ObjectFormMaker.cpp \
    ObjectIdInserter.cpp \
    StringInserter.cpp \
    TimeInserter.cpp \
    UnRegisteredItemInserter.cpp \
    main.cpp \
    MainWidget.cpp

HEADERS += \
    BooleanInserter.h \
    DataItem.h \
    DateInserter.h \
    DateTimeInserter.h \
    DocumentIdInserter.h \
    DoubleInserter.h \
    Elements/ObjectPickerDialog.h \
    Elements/ScrollLessEdits.h \
    Elements/ValidatorDialogue.h \
    Elements/ValidatorIndicatorsSet.h \
    IntegerInserter.h \
    ListFormMaker.h \
    MainWidget.h \
    ObjectFormMaker.h \
    ObjectIdInserter.h \
    StringInserter.h \
    TimeInserter.h \
    UnRegisteredItemInserter.h \
    common.h

FORMS += \
    BooleanInserter.ui \
    DateInserter.ui \
    DateTimeInserter.ui \
    DocumentIdInserter.ui \
    DoubleInserter.ui \
    Elements/ObjectPickerDialog.ui \
    Elements/ValidatorDialogue.ui \
    Elements/ValidatorIndicatorsSet.ui \
    IntegerInserter.ui \
    ListFormMaker.ui \
    MainWidget.ui \
    ObjectFormMaker.ui \
    ObjectIdInserter.ui \
    StringInserter.ui \
    TimeInserter.ui \
    UnRegisteredItemInserter.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

QX_BSON_INCLUDE_PATH=/opt/mongo/drivers/mongoc/include/libbson-1.0
QX_MONGOC_INCLUDE_PATH=/opt/mongo/drivers/mongoc/include/libmongoc-1.0
QX_BSON_LIB_PATH=/opt/mongo/drivers/mongoc/lib/
QX_MONGOC_LIB_PATH=/opt/mongo/drivers/mongoc/lib/

include (/opt/QxOrm/QxOrm.pri)

INCLUDEPATH += /opt/QxOrm/include
LIBS += -L/opt/QxOrm/lib -lQxOrmd

INCLUDEPATH += $$QX_BSON_INCLUDE_PATH
LIBS += -L$$QX_BSON_LIB_PATH -lbson-1.0

unix:CONFIG(release, debug|release): LIBS += -L$$PWD/ExampleClasses/build-ExampleClasses-Desktop-Release/ -lExampleClasses
else:unix:CONFIG(debug, debug|release): LIBS += -L$$PWD/ExampleClasses/build-ExampleClasses-Desktop-Debug/ -lExampleClasses
else:win32:CONFIG(release, debug|release): LIBS += -L$$PWD/ExampleClasses/build-ExampleClasses-Desktop-Release/release/ -lExampleClasses
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/ExampleClasses/build-ExampleClasses-Desktop-Debug/debug/ -lExampleClasses

INCLUDEPATH += $$PWD/ExampleClasses/ExampleClasses
DEPENDPATH += $$PWD/ExampleClasses/ExampleClasses

DISTFILES += \
    config.json
