#include "ObjectPickerDialog.h"
#include "ui_ObjectPickerDialog.h"
#include <QDebug>

QString getOidString (const QJsonValue &data)
{
	QString store;
	store = data.toString();
	if (store.isEmpty() && data.isObject())
	{
		QJsonObject obj = data.toObject();
		if (obj.contains ("$oid"))
		{
			store = obj.value ("$oid").toString();
		}
	}
	return store;
}

ObjectPickerDialog::ObjectPickerDialog (const QString &collectionName, const QString &idKey, const QJsonArray &data, QWidget *parent)
	: QDialog (parent), ui (new Ui::ObjectPickerDialog)
{
	ui->setupUi (this);
	setWindowTitle ("Select object of: " + collectionName);
	QVBoxLayout *layout = new QVBoxLayout;
	for (auto obj1Iter : data)
	{
		if (!obj1Iter.isObject())
		{
			continue;
		}
		QJsonObject obj = obj1Iter.toObject();
		QString oid, details;
		for (QString key : obj.keys())
		{
			if (key == idKey)
			{
				oid = getOidString (obj[key]);
			}
			else
			{
				if (obj[key].isObject())
				{
					details += key + " : " + QString::fromUtf8 (QJsonDocument (obj[key].toObject()).toJson (QJsonDocument::Compact)) + "\n";
				}
				else if (obj[key].isArray())
				{
					details += key + " : " + QString::fromUtf8 (QJsonDocument (obj[key].toArray()).toJson (QJsonDocument::Compact)) + "\n";
				}
				else
				{
					details += key + " : " + obj[key].toVariant().toString() + "\n";
				}
			}
		}
		ObjectEntryWidget *oew = new ObjectEntryWidget (oid, details, this);
		layout->addWidget (oew);
		connect (oew->oidButton, &QPushButton::clicked, this,
			[this, oid] ()
			{
			resultantObjectId = oid;
			close();
		});
	}
	ui->scrollAreaWidgetContents->setLayout (layout);
}

ObjectPickerDialog::~ObjectPickerDialog() { delete ui; }

QString ObjectPickerDialog::getEntrySelection (const QString &collectionName, const QString &idKey, const QJsonArray &data, QWidget *parent)
{
	ObjectPickerDialog modalDialogue (collectionName, idKey, data, parent);
	modalDialogue.exec();
	QString retObjIdString = modalDialogue.getResultantObjectId();
	return retObjIdString;
}

QString ObjectPickerDialog::getResultantObjectId() const { return resultantObjectId; }

ObjectEntryWidget::ObjectEntryWidget (const QString &objId, const QString &details, QWidget *parent) : QWidget (parent)
{
	layout = new QHBoxLayout;
	oidButton = new QPushButton (objId);
	QTextBrowser *detailsBr = new QTextBrowser;
	detailsBr->append (details);
	layout->addWidget (oidButton);
	layout->addWidget (detailsBr);
	setLayout (layout);
}

ObjectEntryWidget::~ObjectEntryWidget() { delete layout; }
