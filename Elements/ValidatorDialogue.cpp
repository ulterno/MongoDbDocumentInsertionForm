#include "ValidatorDialogue.h"
#include "ui_ValidatorDialogue.h"
#include "common.h"
#include <QVBoxLayout>
#include <QGridLayout>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QGroupBox>
#include <QPushButton>
#include <QTextBrowser>
#include <QLabel>

ValidatorDialogue::ValidatorDialogue (QWidget *parent) : QDialog (parent), ui (new Ui::ValidatorDialogue) { ui->setupUi (this); }

ValidatorDialogue::~ValidatorDialogue() { delete ui; }

void mk_tree (QTreeWidgetItem * parent, const QString &key, const QJsonValue &data);

void mk_tree (QTreeWidgetItem * parent, const QJsonValue &data)
{
	switch (data.type())
	{
	case QJsonValue::Bool:
	{
		if (data.toBool())
		{
			parent->setText(1, "true");
			break;
		}
		parent->setText(1, "false");
	}
	break;
	case QJsonValue::String:
	parent->setText(1, data.toString());
	break;
	case QJsonValue::Double:
	parent->setText(1, QString::number(data.toDouble()));
	break;
	case QJsonValue::Array:
	{
		for (auto x : data.toArray())
		{
			mk_tree(parent, x);
		}
	}
	break;
	case QJsonValue::Object:
	{
		for (QString key : data.toObject().keys())
		{
			mk_tree(parent, key, data.toObject()[key]);
		}
	}
	break;
	case QJsonValue::Null:
	{
		parent->setText(1, "Null");
	}
	break;
	default:
	Q_ASSERT(false);
	parent->setText(1, "N/A");
	}
}

void mk_tree (QTreeWidgetItem * parent, const QString &key, const QJsonValue &data)
{
	QTreeWidgetItem * nwi = new QTreeWidgetItem (parent, QStringList () << key);
	QString text;
	switch (data.type())
	{
	case QJsonValue::Bool:
	{
		if (data.toBool())
		{
			text = "true";
			break;
		}
		text = "false";
	}
	break;
	case QJsonValue::String:
	text = data.toString();
	break;
	case QJsonValue::Double:
	text = QString::number(data.toDouble());
	break;
	case QJsonValue::Array:
	{
		int count = 0;
		for (auto x : data.toArray())
		{
			QTreeWidgetItem * mwi = new QTreeWidgetItem (nwi, QStringList () << "[" + QString::number(count++) + "]");
			mk_tree(mwi, QJsonValue(x));
		}
	}
	break;
	case QJsonValue::Object:
	{
		for (QString key : data.toObject().keys())
		{
			mk_tree(nwi, key, data.toObject()[key]);
		}
	}
	break;
	case QJsonValue::Null:
	{
		text = "Null";
	}
	break;
	default:
	text = "N/A";
	}
	nwi->setText(1, text);
	nwi->setExpanded(true);
	return;
}

void mk_tree (QTreeWidgetItem * parent, const QVariant& data)
{
	QString text;
	if (data.canConvert(QMetaType::QString))
	{
		text = data.toString();
		parent->setText(1, text);
		return;
	}
	{
		switch (static_cast<QMetaType::Type>(data.type()))
		{
		case QMetaType::QVariantHash:
		{
			QVariantHash datHash = data.toHash();
			for (QString key : datHash.keys())
			{
				QTreeWidgetItem * nwi = new QTreeWidgetItem (parent, QStringList () << key);
				mk_tree(nwi, datHash[key]);
			}
		}
		break;
		case QMetaType::QJsonArray:
		{
			int count = 0;
			for (auto x : data.toJsonArray())
			{
				QTreeWidgetItem * mwi = new QTreeWidgetItem (parent, QStringList () << "[" + QString::number(count++) + "]");
				mk_tree(mwi, QJsonValue(x));
			}
		}
		break;
		case QMetaType::QJsonObject:
		{
			for (QString key : data.toJsonObject().keys())
			{
				mk_tree(parent, key, data.toJsonObject()[key]);
			}
		}
		break;
		case QMetaType::QJsonValue:
		{
			QTreeWidgetItem * mwi = new QTreeWidgetItem (parent, QStringList () << "➛");
			mk_tree(mwi, data.toJsonValue());
		}
		break;
		case QMetaType::QStringList:
		{
			int count = 0;
			for (auto x : data.toStringList())
			{
				QTreeWidgetItem * mwi = new QTreeWidgetItem (parent, QStringList () << "[" + QString::number(count++) + "]");
				mk_tree(mwi, QJsonValue(x));
			}
		}
		break;
		default:
		Q_ASSERT(false);
		}
	}
}

void make_info_display (const QVariantHash& validators, const QVariantHash& relevantData, const QVariantHash& returnTypeInfo, QLayout *layout)
{
	QTreeWidget *tw = new QTreeWidget;
	QTreeWidgetItem *i1 = new QTreeWidgetItem (tw, QStringList() << "Validator");
	for (QString key : validators.keys())
	{
		QTreeWidgetItem *iIter = new QTreeWidgetItem (i1, QStringList() << key);
		mk_tree(iIter, validators[key]);
	}
	tw->setHeaderLabels(QStringList () << "Field" << "Value");
	tw->setColumnWidth(0, 300);

	i1 = new QTreeWidgetItem (tw, QStringList() << "Relevant Data");
	for (QString key : relevantData.keys())
	{
		QTreeWidgetItem *iIter = new QTreeWidgetItem (i1, QStringList() << key);
		mk_tree(iIter, relevantData[key]);
	}
	tw->setColumnWidth(0, 300);
	tw->setHeaderLabels(QStringList () << "Field" << "Value");

	i1 = new QTreeWidgetItem (tw, QStringList() << "Return Type Info");
	for (QString key : returnTypeInfo.keys())
	{
		QTreeWidgetItem *iIter = new QTreeWidgetItem (i1, QStringList() << key);
		mk_tree(iIter, returnTypeInfo[key]);
	}
	tw->setColumnWidth(0, 300);
	tw->setHeaderLabels(QStringList () << "Field" << "Value");
	layout->addWidget(tw);
}

void make_validator_display (const QVariantHash& validators, const QVariantHash& relevantData, const QVariantHash& returnTypeInfo, QLayout *layout, ValidatorDialogue *the);
void make_equal_other_doc_widget (const QVariantHash& validators, const QVariantHash& relevantData, const QVariantHash& returnTypeInfo, QGridLayout *layout, ValidatorDialogue *the);

QVariantHash ValidatorDialogue::showValidatorWidget(const QVariantHash &validators, const QVariantHash &relevantData, const QVariantHash &returnTypeInfo)
{
	ValidatorDialogue vmd;
	if (relevantData.contains("$$me"))
	{
		vmd.setWindowTitle("Validator \"" + relevantData["$$me"].toString() + "\"");
	}
	QVBoxLayout *infoLayout = new QVBoxLayout;
	make_info_display(validators, relevantData, returnTypeInfo, infoLayout);

	QVBoxLayout *widgetLayout = new QVBoxLayout;
	make_validator_display(validators, relevantData, returnTypeInfo, widgetLayout, &vmd);

	vmd.ui->sAWC_ValidatorInfo->setLayout(infoLayout);
	vmd.ui->sAWC_ValidatorWidget->setLayout(widgetLayout);

	vmd.exec();
	return vmd.m_userInputResult;
}

void make_validator_display (const QVariantHash& validators, const QVariantHash& relevantData, const QVariantHash& returnTypeInfo, QLayout *layout, ValidatorDialogue *the)
{
	if (validators["$$this"].toJsonObject().contains("equal other doc"))
	{
		QGroupBox * gB_EqualOtherDoc = new QGroupBox ("equal other doc");
		QGridLayout * eodContentLayout = new QGridLayout (gB_EqualOtherDoc);
		make_equal_other_doc_widget(validators, relevantData, returnTypeInfo, eodContentLayout, the);

		layout->addWidget(gB_EqualOtherDoc);
	}
}

void make_equal_other_doc_widget (const QVariantHash& validators, const QVariantHash& relevantData, const QVariantHash& returnTypeInfo, QGridLayout *layout, ValidatorDialogue *the)
{
	QJsonObject eod_obj = validators["$$this"].toJsonObject()["equal other doc"].toObject();
	bool single_local{};
	if (eod_obj["local field for relation"].toString() == relevantData["$$me"].toString())
	{
		single_local = true;
	}
	QString current_field = eod_obj["remote field"].toString();
	QString related_field = eod_obj["remote field for relation"].toString();
	QJsonArray doc = relevantData["doc"].toJsonArray();
	int grid_row_count {};
	{
		int grid_column_count {};
		layout->addWidget(new QLabel ("This: '" + current_field + "'"), grid_row_count, grid_column_count);
		grid_column_count++;
		if (!single_local)
		{
			layout->addWidget(new QLabel ("Related: '" + related_field + "'"), grid_row_count, grid_column_count++);
		}
		layout->addWidget(new QLabel ("Rest..."), grid_row_count, grid_column_count, 1, 2, Qt::AlignCenter);
		grid_column_count++;
		grid_row_count++;
	}
	for (auto di : doc)
	{
		int grid_column_count {};
		if (!di.isObject())
		{
			continue;
		}
		QJsonObject a_obj = di.toObject();
		QJsonValue val = a_obj[current_field];
		QString valString (val.toVariant().toString());
		if (val.isObject())
		{
			valString = val.toObject()["$oid"].toString();
		}
		QPushButton *cfButton = new QPushButton (valString);
		layout->addWidget(cfButton, grid_row_count, grid_column_count);
		grid_column_count++;
		QObject::connect(cfButton, &QPushButton::clicked, the, [the, val] ()
		{
			the->m_userInputResult.insert("fill current", val);
			the->close();
		});
		if (!single_local)
		{
			QJsonValue val = a_obj[related_field];
			QString valString (val.toVariant().toString());
			if (val.isObject())
			{
				valString = val.toObject()["$oid"].toString();
			}
			QPushButton *rfButton = new QPushButton (valString);
			layout->addWidget(rfButton, grid_row_count, grid_column_count);
			grid_column_count++;
			QObject::connect(rfButton, &QPushButton::clicked, the, [the, val] ()
			{
				the->m_userInputResult.insert("fill related", val);
				the->close();
			});
		}
		QPushButton *allButton = new QPushButton ("Fill");
		layout->addWidget(allButton, grid_row_count, grid_column_count);
		grid_column_count++;
		QTextBrowser *tbAllInfo = new QTextBrowser;
		tbAllInfo->append(QString::fromUtf8(QJsonDocument(a_obj).toJson()));
		tbAllInfo->setMaximumHeight(100);
		layout->addWidget(tbAllInfo, grid_row_count, grid_column_count);
		grid_column_count++;
		QObject::connect(allButton, &QPushButton::clicked, the, [the, a_obj] ()
		{
			the->m_userInputResult.insert("fill all", a_obj);
			the->close();
		});
		grid_row_count++;
	}
}
