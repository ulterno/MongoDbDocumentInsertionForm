#ifndef SCROLLLESSEDITS_H
#define SCROLLLESSEDITS_H

#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QDateTimeEdit>
#include <QDateEdit>
#include <QTimeEdit>
#include <QWheelEvent>

class ScrollLessSpinBox : public QSpinBox
{
	Q_OBJECT
public:
	ScrollLessSpinBox (QWidget *parent);

protected:
	void wheelEvent (QWheelEvent *event) override;
};

class ScrollLessDoubleSpinBox : public QDoubleSpinBox
{
	Q_OBJECT
public:
	ScrollLessDoubleSpinBox (QWidget *parent);

protected:
	void wheelEvent (QWheelEvent *event) override;
};

class ScrollLessDateTimeEdit : public QDateTimeEdit
{
	Q_OBJECT
public:
	ScrollLessDateTimeEdit (QWidget *parent);

protected:
	void wheelEvent (QWheelEvent *event) override;
};

class ScrollLessDateEdit : public QDateEdit
{
	Q_OBJECT
public:
	ScrollLessDateEdit (QWidget *parent);

protected:
	void wheelEvent (QWheelEvent *event) override;
};

class ScrollLessTimeEdit : public QTimeEdit
{
	Q_OBJECT
public:
	ScrollLessTimeEdit (QWidget *parent);

protected:
	void wheelEvent (QWheelEvent *event) override;
};
#endif		  // SCROLLLESSEDITS_H
