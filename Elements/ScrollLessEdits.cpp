#include "ScrollLessEdits.h"

ScrollLessSpinBox::ScrollLessSpinBox (QWidget *parent) : QSpinBox (parent) {}

void ScrollLessSpinBox::wheelEvent (QWheelEvent *event) { event->ignore(); }

ScrollLessDoubleSpinBox::ScrollLessDoubleSpinBox (QWidget *parent) : QDoubleSpinBox (parent) {}

void ScrollLessDoubleSpinBox::wheelEvent (QWheelEvent *event) { event->ignore(); }

ScrollLessDateTimeEdit::ScrollLessDateTimeEdit(QWidget *parent) : QDateTimeEdit (parent)
{

}

void ScrollLessDateTimeEdit::wheelEvent(QWheelEvent *event)
{
	event->ignore();
}

ScrollLessDateEdit::ScrollLessDateEdit(QWidget *parent) : QDateEdit (parent)
{

}

void ScrollLessDateEdit::wheelEvent(QWheelEvent *event)
{
	event->ignore();
}

ScrollLessTimeEdit::ScrollLessTimeEdit(QWidget *parent) : QTimeEdit (parent)
{

}

void ScrollLessTimeEdit::wheelEvent(QWheelEvent *event)
{
	event->ignore();
}
