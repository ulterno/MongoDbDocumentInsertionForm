#ifndef OBJECTPICKERDIALOG_H
#define OBJECTPICKERDIALOG_H

#include "common.h"
#include <QDialog>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QTextBrowser>

class ObjectEntryWidget : public QWidget
{
	Q_OBJECT
public:
	explicit ObjectEntryWidget (const QString& objId, const QString& details, QWidget* parent = nullptr);
	virtual ~ObjectEntryWidget();

	QPushButton* oidButton;

private:
	QHBoxLayout* layout;
};

namespace Ui
{
class ObjectPickerDialog;
}

class ObjectPickerDialog : public QDialog
{
	Q_OBJECT

public:
	ObjectPickerDialog (const QString& collectionName, const QString& idKey, const QJsonArray& data, QWidget* parent = nullptr);
	~ObjectPickerDialog();

	static QString getEntrySelection (const QString& collectionName, const QString& idKey, const QJsonArray& data, QWidget* parent = nullptr);

	QString getResultantObjectId () const;

private:
	Ui::ObjectPickerDialog* ui;
	QString resultantObjectId;
};

#endif		  // OBJECTPICKERDIALOG_H
