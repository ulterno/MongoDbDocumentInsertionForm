#ifndef VALIDATORDIALOGUE_H
#define VALIDATORDIALOGUE_H

#include <QDialog>

namespace Ui
{
class ValidatorDialogue;
}

class ValidatorDialogue : public QDialog
{
	Q_OBJECT

public:
	explicit ValidatorDialogue (QWidget *parent = nullptr);
	virtual ~ValidatorDialogue();

	static QVariantHash showValidatorWidget (const QVariantHash &validators, const QVariantHash &relevantData, const QVariantHash &returnTypeInfo);
	QVariantHash m_userInputResult;
private:
	Ui::ValidatorDialogue *ui;
};

#endif		  // VALIDATORDIALOGUE_H
