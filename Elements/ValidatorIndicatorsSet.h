#ifndef VALIDATORINDICATORSSET_H
#define VALIDATORINDICATORSSET_H

#include <QWidget>

namespace Ui {
class ValidatorIndicatorsSet;
}

class ValidatorIndicatorsSet : public QWidget
{
	Q_OBJECT

public:
	explicit ValidatorIndicatorsSet(QWidget *parent = nullptr);
	~ValidatorIndicatorsSet();

	void setValidity (bool capsValid, bool forbiddenCharsValid, bool eodValid);

	void setCapsValid (bool isValid);
	void setForbiddenValid (bool isValid);
	void setEqualOtherDocValid (bool isValid);

private:
	Ui::ValidatorIndicatorsSet *ui;
	struct State
	{
		bool caps = true;
		bool forbiddenChars = true;
		bool equalOtherDoc = true;

		bool capsSet{};
		bool forbiddenCharsSet{};
		bool equalOtherDocSet{};
	} m_state{};
	void updateIndicators ();
};

#endif // VALIDATORINDICATORSSET_H
