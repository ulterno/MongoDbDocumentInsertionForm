#include "ValidatorIndicatorsSet.h"
#include "ui_ValidatorIndicatorsSet.h"

ValidatorIndicatorsSet::ValidatorIndicatorsSet(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::ValidatorIndicatorsSet)
{
	ui->setupUi(this);
	setValidity(true, true, true);
}

ValidatorIndicatorsSet::~ValidatorIndicatorsSet()
{
	delete ui;
}

void ValidatorIndicatorsSet::setValidity(bool capsValid, bool forbiddenCharsValid, bool eodValid)
{
	m_state.caps = capsValid;
	m_state.forbiddenChars = forbiddenCharsValid;
	m_state.equalOtherDoc = eodValid;
	updateIndicators();
}

void ValidatorIndicatorsSet::setCapsValid(bool isValid)
{
	m_state.caps = isValid;
	m_state.capsSet = true;
	updateIndicators();
}

void ValidatorIndicatorsSet::setForbiddenValid(bool isValid)
{
	m_state.forbiddenChars = isValid;
	m_state.forbiddenCharsSet = true;
	updateIndicators();
}

void ValidatorIndicatorsSet::setEqualOtherDocValid(bool isValid)
{
	m_state.equalOtherDoc = isValid;
	m_state.equalOtherDocSet = true;
	updateIndicators();
}

void ValidatorIndicatorsSet::updateIndicators()
{
	setUpdatesEnabled(false);
	/// \badcode Make this logic better
	if (!m_state.capsSet)
	{
		ui->l_Caps->hide();
	}
	if (!m_state.forbiddenCharsSet)
	{
		ui->l_ForbiddedCharacters->hide();
	}
	if (!m_state.equalOtherDocSet)
	{
		ui->l_EqualOtherDoc->hide();
	}
	if (m_state.caps && m_state.equalOtherDoc && m_state.forbiddenChars)
	{
		ui->l_EqualOtherDoc->setStyleSheet("QLabel{ background-color : #00ff00; color : #000000; }");
		ui->l_Caps->setStyleSheet("QLabel{ background-color : #00ff00; color : #000000; }");
		ui->l_ForbiddedCharacters->setStyleSheet("QLabel{ background-color : #00ff00; color : #000000; }");
		ui->l_EqualOtherDoc->setVisible(true);
	}
	else
	{
		if (!m_state.caps)
		{
			ui->l_Caps->setStyleSheet("QLabel{ background-color : #ff0000; }");
			ui->l_Caps->setVisible(true);
		}
		else
		{
			ui->l_Caps->setStyleSheet("QLabel{ background-color : #00ee00;  color : #000000; }");
		}
		if (!m_state.forbiddenChars)
		{
			ui->l_ForbiddedCharacters->setStyleSheet("QLabel{ background-color : #ff0000; }");
			ui->l_ForbiddedCharacters->setVisible(true);
		}
		else
		{
			ui->l_ForbiddedCharacters->setStyleSheet("QLabel{ background-color : #00ee00;  color : #000000; }");
		}
		if (!m_state.equalOtherDoc && m_state.equalOtherDocSet)
		{
			ui->l_EqualOtherDoc->setStyleSheet("QLabel{ background-color : #ff0000; }");
			ui->l_EqualOtherDoc->setVisible(true);
		}
		else
		{
			ui->l_EqualOtherDoc->setStyleSheet("QLabel{ background-color : #00ee00;  color : #000000; }");
		}
	}
	setUpdatesEnabled(true);
}
