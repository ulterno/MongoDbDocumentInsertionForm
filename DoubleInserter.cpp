#include "DoubleInserter.h"
#include "ui_DoubleInserter.h"
#include "Elements/ValidatorDialogue.h"

DoubleInserter::DoubleInserter (const QString &keyName, const WidgetConfiguration &config, QWidget *parent)
	: QWidget (parent), DataItem (keyName), ui (new Ui::DoubleInserter), m_config (config)
{
	ui->setupUi (this);
	ui->label_Key->setText (keyName);
	ui->cB_Enable->setChecked (!config.disableByDefault);
	connect (ui->sB_Precision, QOverload<int>::of (&QSpinBox::valueChanged), ui->dSB_Value, &QDoubleSpinBox::setDecimals);
	if (!config.disableByDefault)
	{
		connect (ui->cB_Enable, &QCheckBox::toggled, this,
			[this] (bool checked)
			{
			if (checked)
			{
				ui->cB_Enable->setText ("");
				ui->cB_Enable->setStyleSheet ("");
				ui->cB_Enable->setMaximumWidth (32);
			}
			else if (ui->cB_Enable->isEnabled())
			{
				ui->cB_Enable->setText ("THIS KEY WILL BE DELETED ! (If you see red)");
				ui->cB_Enable->setStyleSheet ("*:enabled{ color : Red ; }");
				ui->cB_Enable->setMaximumWidth (400);
			}
		});
	}
	connect (this, &DoubleInserter::validatorChanged, this,
		[this] (const QVariantHash &)
		{
		todo.validators_done = false;
		scriptControl();
	});
}

DoubleInserter::~DoubleInserter() { delete ui; }

QJsonValue DoubleInserter::getValueData()
{
	if (!ui->dSB_Value->isEnabled())
	{
		return QJsonValue (QJsonValue::Null);
	}
	return ui->dSB_Value->value();
}

bool DoubleInserter::setValueData (const QJsonValue &data)
{
	/// Set Precision
	QVariant datVar = data.toVariant();
	if (datVar.convert (QMetaType::QString))
	{
		QString datString = datVar.toString();
		int decimalIndex = datString.indexOf ('.');
		if (decimalIndex < 0)
		{
			ui->sB_Precision->setValue (3);
		}
		else
		{
			int digitsAfterDecimal = datString.length() - decimalIndex - 1;
			if (digitsAfterDecimal <= 0)
			{
				ui->sB_Precision->setValue (3);
			}
			else
			{
				ui->sB_Precision->setValue (digitsAfterDecimal);
			}
		}
	}
	/// Set Value
	bool retVal;
	double store;
	retVal = qx::cvt::detail::QxConvert_FromJson<double>::fromJson (data, store, QString());
	if (retVal)
	{
		ui->dSB_Value->setValue (store);
	}
	b_failedSettingData = !retVal;
	return retVal;
}

void DoubleInserter::runValidator (const QVariantHash &validatorData) { runEqualOtherDoc (validatorData); }

void DoubleInserter::applyValidators()
{
	QVariantHash validators = getValidators();
	if (validators.isEmpty())
	{
		return;
	}
	QJsonObject valsOfThis = validators["$$this"].toJsonObject();
	if (valsOfThis.isEmpty())
	{
		return;
	}
	if (valsOfThis.contains ("equal other doc"))
	{
		QJsonObject equal_other_doc_options = valsOfThis["equal other doc"].toObject();
		if (equal_other_doc_options.contains ("collection"))
		{
			// Make equal_other_doc options
			// Send document request
			QVariantHash eodRequest;
			eodRequest.insert ("equal other doc", valsOfThis["equal other doc"].toObject());
			eodRequest.insert ("$$me", getKey());
			// Set receiver on
			todo.equal_other_doc = true;
			emit validatorSignal (eodRequest);
		}
		connect (ui->dSB_Value, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &DoubleInserter::equalOtherDocValidatorInstance, Qt::UniqueConnection);
	}
	else
	{
		disconnect (ui->dSB_Value, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &DoubleInserter::equalOtherDocValidatorInstance);
	}
}

bool DoubleInserter::getCase_invalid() const
{
	return todo.indicate.case_invalid;
}

void DoubleInserter::setCase_invalid(bool newCase_invalid)
{
	ui->w_Indicators->setCapsValid(!newCase_invalid);
	todo.indicate.case_invalid = newCase_invalid;
}

bool DoubleInserter::getForbidden_characters_invalid() const
{
	return todo.indicate.forbidden_characters_invalid;
}

void DoubleInserter::setForbidden_characters_invalid(bool newForbidden_characters_invalid)
{
	ui->w_Indicators->setForbiddenValid(!newForbidden_characters_invalid);
	todo.indicate.forbidden_characters_invalid = newForbidden_characters_invalid;
}

bool DoubleInserter::getEqual_other_doc_invalid() const
{
	return todo.indicate.equal_other_doc_invalid;
}

void DoubleInserter::setEqual_other_doc_invalid(bool newEqual_other_doc_invalid)
{
	ui->w_Indicators->setEqualOtherDocValid(!newEqual_other_doc_invalid);
	todo.indicate.equal_other_doc_invalid = newEqual_other_doc_invalid;
}

void DoubleInserter::scriptControl()
{
	if (todo.validators_done)
	{
		return;
	}
	// Do Validator stuff
	applyValidators();
	todo.validators_done = true;
}

void DoubleInserter::equalOtherDocValidatorInstance(const double &currentData)
{
	if (m_currentCompletionList.contains (currentData))
	{
		setEqual_other_doc_invalid(false);
		return;
	}
	setEqual_other_doc_invalid(true);
}

void DoubleInserter::runEqualOtherDoc(const QVariantHash &validatorData)
{
	QVariantHash validators = getValidators();
	QJsonObject eodObj = validators["$$this"].toJsonObject()["equal other doc"].toObject();
	if (todo.equal_other_doc && validatorData.contains ("equal other doc"))
	{
		todo.equal_other_doc = false;
		QJsonObject eodReturn = validatorData["equal other doc"].toJsonObject();
		QJsonArray otherDocsData = eodReturn["data"].toArray();
		m_storedOtherDocsData = otherDocsData;
		if (otherDocsData.isEmpty())
		{
			m_currentCompletionList.clear();
			return;
		}
		for (auto objIter : otherDocsData)
		{
			if (!objIter.isObject())
			{
				continue;
			}
			QJsonObject aObj = objIter.toObject();
			if (aObj.contains (eodObj["remote field"].toString()))		  // ToDo: Make logic for cases where fields are not top level
			{
				m_currentCompletionList.append (aObj[eodObj["remote field"].toString()].toDouble());
			}
		}
	}
}

void DoubleInserter::on_pB_Validator_clicked()
{
	QVariantHash relevantData;
	if (!m_storedOtherDocsData.isEmpty())
	{
		relevantData.insert("doc", m_storedOtherDocsData);
	}
	relevantData.insert("$$me", getKey());
	relevantData.insert("current value", getValueData());
	QVariantHash returnTypeData;
	returnTypeData.insert("type", "string");
	QVariantHash userRetVal = ValidatorDialogue::showValidatorWidget(getValidators(), relevantData, returnTypeData);
	userRetVal.insert("$$me", getKey());
	emit validatorSignal(userRetVal);
}

