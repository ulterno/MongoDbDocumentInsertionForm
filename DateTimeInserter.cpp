#include "DateTimeInserter.h"
#include "ui_DateTimeInserter.h"
#include "Elements/ValidatorDialogue.h"

#include <QJsonObject>
#include <QDateTime>

DateTimeInserter::DateTimeInserter (const QString &keyName, const WidgetConfiguration &config, QWidget *parent)
	: QWidget (parent), DataItem (keyName), ui (new Ui::DateTimeInserter), m_config (config)
{
	ui->setupUi (this);
	ui->label_Key->setText (keyName);
	ui->dateTimeEdit->setDateTime (QDateTime::currentDateTimeUtc());
	connect (ui->cB_TimeSpec, QOverload<int>::of (&QComboBox::currentIndexChanged), this, &DateTimeInserter::setTimeSpec);
	connect (ui->pB_CurrentTime, &QPushButton::clicked, this, &DateTimeInserter::setCurrentTime);
	ui->cB_Enable->setChecked (!config.disableByDefault);
	if (!config.disableByDefault)
	{
		connect (ui->cB_Enable, &QCheckBox::toggled, this,
			[this] (bool checked)
			{
			if (checked)
			{
				ui->cB_Enable->setText ("");
				ui->cB_Enable->setStyleSheet ("");
				ui->cB_Enable->setMaximumWidth (32);
			}
			else if (ui->cB_Enable->isEnabled())
			{
				ui->cB_Enable->setText ("THIS KEY WILL BE DELETED ! (If you see red)");
				ui->cB_Enable->setStyleSheet ("*:enabled{ color : Red ; }");
				ui->cB_Enable->setMaximumWidth (400);
			}
		});
	}
	connect (this, &DateTimeInserter::validatorChanged, this,
		[this] (const QVariantHash &)
		{
		todo.validators_done = false;
		scriptControl();
	});
}

DateTimeInserter::~DateTimeInserter() { delete ui; }

QJsonValue DateTimeInserter::getValueData()
{
	if (!ui->dateTimeEdit->isEnabled())
	{
		return QJsonValue (QJsonValue::Null);
	}
	QJsonObject retVal;
	retVal.insert ("$date", ui->dateTimeEdit->dateTime().toUTC().toString (Qt::ISODateWithMs));
	return retVal;
}

bool DateTimeInserter::setValueData (const QJsonValue &data)
{
	bool retVal;
	QDateTime store;
	retVal = qx::cvt::detail::QxConvert_FromJson<QDateTime>::fromJson (data, store, QString());
	if (retVal)
	{
		ui->dateTimeEdit->setDateTime (store);
	}
	b_failedSettingData = !retVal;
	return retVal;
}

void DateTimeInserter::runValidator (const QVariantHash &validatorData) { runEqualOtherDoc (validatorData); }

void DateTimeInserter::setTimeSpec (const int &enumeratedValue) { ui->dateTimeEdit->setTimeSpec (static_cast<Qt::TimeSpec> (enumeratedValue)); }

void DateTimeInserter::setCurrentTime()
{
	Qt::TimeSpec timeSpec = static_cast<Qt::TimeSpec> (ui->cB_TimeSpec->currentIndex());
	ui->dateTimeEdit->setDateTime (QDateTime::currentDateTimeUtc().toTimeSpec (timeSpec));
}

void DateTimeInserter::applyValidators()
{
	QVariantHash validators = getValidators();
	if (validators.isEmpty())
	{
		return;
	}
	QJsonObject valsOfThis = validators["$$this"].toJsonObject();
	if (valsOfThis.isEmpty())
	{
		return;
	}
	if (valsOfThis.contains ("equal other doc"))
	{
		QJsonObject equal_other_doc_options = valsOfThis["equal other doc"].toObject();
		if (equal_other_doc_options.contains ("collection"))
		{
			// Make equal_other_doc options
			// Send document request
			QVariantHash eodRequest;
			eodRequest.insert ("equal other doc", valsOfThis["equal other doc"].toObject());
			eodRequest.insert ("$$me", getKey());
			// Set receiver on
			todo.equal_other_doc = true;
			emit validatorSignal (eodRequest);
		}
		connect (ui->dateTimeEdit, &QDateEdit::editingFinished, this, &DateTimeInserter::equalOtherDocValidatorInstance, Qt::UniqueConnection);
	}
	else
	{
		disconnect (ui->dateTimeEdit, &QDateEdit::editingFinished, this, &DateTimeInserter::equalOtherDocValidatorInstance);
	}
}

bool DateTimeInserter::getCase_invalid() const
{
	return todo.indicate.case_invalid;
}

void DateTimeInserter::setCase_invalid(bool newCase_invalid)
{
	ui->w_Indicators->setCapsValid(!newCase_invalid);
	todo.indicate.case_invalid = newCase_invalid;
}

bool DateTimeInserter::getForbidden_characters_invalid() const
{
	return todo.indicate.forbidden_characters_invalid;
}

void DateTimeInserter::setForbidden_characters_invalid(bool newForbidden_characters_invalid)
{
	ui->w_Indicators->setForbiddenValid(!newForbidden_characters_invalid);
	todo.indicate.forbidden_characters_invalid = newForbidden_characters_invalid;
}

bool DateTimeInserter::getEqual_other_doc_invalid() const
{
	return todo.indicate.equal_other_doc_invalid;
}

void DateTimeInserter::setEqual_other_doc_invalid(bool newEqual_other_doc_invalid)
{
	ui->w_Indicators->setEqualOtherDocValid(!newEqual_other_doc_invalid);
	todo.indicate.equal_other_doc_invalid = newEqual_other_doc_invalid;
}

void DateTimeInserter::scriptControl()
{
	if (todo.validators_done)
	{
		return;
	}
	// Do Validator stuff
	applyValidators();
	todo.validators_done = true;
}

void DateTimeInserter::equalOtherDocValidatorInstance()
{
	if (m_currentCompletionList.contains (getValueData()))
	{
		setEqual_other_doc_invalid(false);
		return;
	}
	setEqual_other_doc_invalid(true);
}

void DateTimeInserter::runEqualOtherDoc(const QVariantHash &validatorData)
{
	QVariantHash validators = getValidators();
	QJsonObject eodObj = validators["$$this"].toJsonObject()["equal other doc"].toObject();
	if (todo.equal_other_doc && validatorData.contains ("equal other doc"))
	{
		todo.equal_other_doc = false;
		QJsonObject eodReturn = validatorData["equal other doc"].toJsonObject();
		QJsonArray otherDocsData = eodReturn["data"].toArray();
		m_storedOtherDocsData = otherDocsData;
		if (otherDocsData.isEmpty())
		{
			m_currentCompletionList.clear();
			return;
		}
		for (auto objIter : otherDocsData)
		{
			if (!objIter.isObject())
			{
				continue;
			}
			QJsonObject aObj = objIter.toObject();
			if (aObj.contains (eodObj["remote field"].toString()))		  // ToDo: Make logic for cases where fields are not top level
			{
				m_currentCompletionList.append (aObj[eodObj["remote field"].toString()]);
			}
		}
	}
}

void DateTimeInserter::on_pB_Validator_clicked()
{
	QVariantHash relevantData;
	if (!m_storedOtherDocsData.isEmpty())
	{
		relevantData.insert("doc", m_storedOtherDocsData);
	}
	relevantData.insert("$$me", getKey());
	relevantData.insert("current value", getValueData());
	QVariantHash returnTypeData;
	returnTypeData.insert("type", "string");
	QVariantHash userRetVal = ValidatorDialogue::showValidatorWidget(getValidators(), relevantData, returnTypeData);
	userRetVal.insert("$$me", getKey());
	emit validatorSignal(userRetVal);
}

