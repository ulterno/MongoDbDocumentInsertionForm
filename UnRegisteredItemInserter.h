#ifndef UNREGISTEREDITEMINSERTER_H
#define UNREGISTEREDITEMINSERTER_H

#include <QWidget>
#include "DataItem.h"

namespace Ui
{
class UnRegisteredItemInserter;
}

class UnRegisteredItemInserter : public QWidget, public DataItem
{
	Q_OBJECT

public:
	explicit UnRegisteredItemInserter (const QString &keyName, const DataItem::WidgetConfiguration &config, QWidget *parent = nullptr);
	virtual ~UnRegisteredItemInserter();

	QJsonValue getValueData () override;
	bool setValueData (const QJsonValue &data) override;

	void runValidator (const QVariantHash &validatorData) override;

signals:
	void validatorSignal (QVariantHash info) override;
	void validatorChanged (const QVariantHash &changedValue) override;

private:
	Ui::UnRegisteredItemInserter *ui;
	DataItem::WidgetConfiguration m_config;
	QJsonValue::Type m_currentDataType;
};

#endif		  // UNREGISTEREDITEMINSERTER_H
