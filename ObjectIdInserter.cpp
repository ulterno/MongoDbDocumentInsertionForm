#include "ObjectIdInserter.h"
#include "ui_ObjectIdInserter.h"
#include "Elements/ValidatorDialogue.h"

#include <QJsonObject>
#include <QCompleter>

ObjectIdInserter::ObjectIdInserter (const QString &keyName, const WidgetConfiguration &config, QWidget *parent)
	: QWidget (parent), DataItem (keyName), ui (new Ui::ObjectIdInserter), m_config (config)
{
	ui->setupUi (this);
	ui->label_Key->setText (keyName);
	ui->cB_Enable->setChecked (!config.disableByDefault);
	if (!config.disableByDefault)
	{
		connect (ui->cB_Enable, &QCheckBox::toggled, this,
			[this] (bool checked)
			{
			if (checked)
			{
				ui->cB_Enable->setText ("");
				ui->cB_Enable->setStyleSheet ("");
				ui->cB_Enable->setMaximumWidth (32);
			}
			else if (ui->cB_Enable->isEnabled())
			{
				ui->cB_Enable->setText ("THIS KEY WILL BE DELETED ! (If you see red)");
				ui->cB_Enable->setStyleSheet ("*:enabled{ color : Red ; }");
				ui->cB_Enable->setMaximumWidth (400);
			}
		});
	}
	connect (this, &ObjectIdInserter::validatorChanged, this,
		[this] (const QVariantHash &)
		{
		todo.validators_done = false;
		scriptControl();
	});
}

ObjectIdInserter::~ObjectIdInserter() { delete ui; }

QJsonValue ObjectIdInserter::getValueData()
{
	/// If Viable Object ID, then enter as $oid, else, anter plain text id
	if (!ui->lE_ObjectId->isEnabled() || ui->lE_ObjectId->text().isEmpty())
	{
		return QJsonValue (QJsonValue::Null);
	}
	QString idStr = ui->lE_ObjectId->text();
	if (idStr.length() == 24)
	{
		bool viableOid = true;
		for (QChar aChar : idStr)
		{
			if (aChar.isDigit())
			{
				continue;
			}
			if (aChar.isLetter())
			{
				switch (aChar.toUpper().toLatin1())
				{
				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
					continue;
				default:
					viableOid = false;
					break;
				}
			}
		}
		if (viableOid)
		{
			QJsonObject objectId;
			objectId.insert ("$oid", ui->lE_ObjectId->text());
			return objectId;
		}
		return idStr;
	}
	return idStr;
}

bool ObjectIdInserter::setValueData (const QJsonValue &data)
{
	bool retVal;
	ObjectIdType store;
	retVal = qx::cvt::detail::QxConvert_FromJson<ObjectIdType>::fromJson (data, store, QString());
	if (retVal)
	{
		ui->lE_ObjectId->setText (store["$oid"]);
	}
	b_failedSettingData = !retVal;
	return retVal;
}

void ObjectIdInserter::runValidator (const QVariantHash &validatorData) { runEqualOtherDoc (validatorData); }

void ObjectIdInserter::applyValidators()
{
	QVariantHash validators = getValidators();
	if (validators.isEmpty())
	{
		return;
	}
	QJsonObject valsOfThis = validators["$$this"].toJsonObject();
	if (valsOfThis.isEmpty())
	{
		return;
	}
	if (valsOfThis.contains ("case"))
	{
		QString typeCase = valsOfThis["case"].toString();
		if (typeCase == "capital")
		{
			// Restrict entered data to capital
			todo.validate_case = ToDo::VC_Caps;
			connect (ui->lE_ObjectId, &QLineEdit::textChanged, this, &ObjectIdInserter::caseValidatorInstance, Qt::UniqueConnection);
		}
		else if (typeCase == "small")
		{
			// Restrict entered data to small
			todo.validate_case = ToDo::VC_Small;
			connect (ui->lE_ObjectId, &QLineEdit::textChanged, this, &ObjectIdInserter::caseValidatorInstance, Qt::UniqueConnection);
		}
		else
		{
			// Remove validator
			disconnect (ui->lE_ObjectId, &QLineEdit::textChanged, this, &ObjectIdInserter::caseValidatorInstance);
		}
	}
	if (valsOfThis.contains ("forbidden characters"))
	{
		QJsonArray forbidden_characters = valsOfThis["forbidden characters"].toArray();
		// Apply Forbidden Characters
		for (auto aChar : forbidden_characters)
		{
			todo.forbidden_characters.append (aChar.toInt());
		}
		connect (ui->lE_ObjectId, &QLineEdit::textChanged, this, &ObjectIdInserter::forbiddenCharactersValidatorInstance, Qt::UniqueConnection);
	}
	else
	{
		disconnect (ui->lE_ObjectId, &QLineEdit::textChanged, this, &ObjectIdInserter::forbiddenCharactersValidatorInstance);
	}
	if (valsOfThis.contains ("equal other doc"))
	{
		QJsonObject equal_other_doc_options = valsOfThis["equal other doc"].toObject();
		if (equal_other_doc_options.contains ("collection"))
		{
			// Make equal_other_doc options
			// Send document request
			QVariantHash eodRequest;
			eodRequest.insert ("equal other doc", valsOfThis["equal other doc"].toObject());
			eodRequest.insert ("$$me", getKey());
			// Set receiver on
			todo.equal_other_doc = true;
			emit validatorSignal (eodRequest);
		}
		connect (ui->lE_ObjectId, &QLineEdit::textChanged, this, &ObjectIdInserter::equalOtherDocValidatorInstance, Qt::UniqueConnection);
	}
	else
	{
		disconnect (ui->lE_ObjectId, &QLineEdit::textChanged, this, &ObjectIdInserter::equalOtherDocValidatorInstance);
	}
}

bool ObjectIdInserter::getCase_invalid() const
{
	return todo.indicate.case_invalid;
}

void ObjectIdInserter::setCase_invalid(bool newCase_invalid)
{
	ui->w_Indicators->setCapsValid(!newCase_invalid);
	todo.indicate.case_invalid = newCase_invalid;
}

bool ObjectIdInserter::getForbidden_characters_invalid() const
{
	return todo.indicate.forbidden_characters_invalid;
}

void ObjectIdInserter::setForbidden_characters_invalid(bool newForbidden_characters_invalid)
{
	ui->w_Indicators->setForbiddenValid(!newForbidden_characters_invalid);
	todo.indicate.forbidden_characters_invalid = newForbidden_characters_invalid;
}

bool ObjectIdInserter::getEqual_other_doc_invalid() const
{
	return todo.indicate.equal_other_doc_invalid;
}

void ObjectIdInserter::setEqual_other_doc_invalid(bool newEqual_other_doc_invalid)
{
	ui->w_Indicators->setEqualOtherDocValid(!newEqual_other_doc_invalid);
	todo.indicate.equal_other_doc_invalid = newEqual_other_doc_invalid;
}

void ObjectIdInserter::scriptControl()
{
	if (todo.validators_done)
	{
		return;
	}
	// Do Validator stuff
	applyValidators();
	todo.validators_done = true;
}

void ObjectIdInserter::forbiddenCharactersValidatorInstance(const QString &lineEditText)
{
	for (QChar fChar : todo.forbidden_characters)
	{
		if (lineEditText.contains (fChar))
		{
			setForbidden_characters_invalid(true);
			return;
		}
	}
	setForbidden_characters_invalid(false);
}

void ObjectIdInserter::caseValidatorInstance(const QString &lineEditText)
{
	switch (todo.validate_case)
	{
	case ToDo::VC_Caps:
	{
		if (lineEditText.toUpper() == lineEditText)
		{
			setCase_invalid(false);
			return;
		}
		setCase_invalid(true);
	}
	break;
	case ToDo::VC_Small:
	{
		if (lineEditText.toLower() == lineEditText)
		{
			setCase_invalid(false);
			return;
		}
		setCase_invalid(true);
	}
	break;
	default:
		return;
	}
}

void ObjectIdInserter::equalOtherDocValidatorInstance(const QString &lineEditText)
{
	if (m_currentCompletionList.contains (lineEditText))
	{
		setEqual_other_doc_invalid(false);
		return;
	}
	setEqual_other_doc_invalid(true);
}

void ObjectIdInserter::runEqualOtherDoc(const QVariantHash &validatorData)
{
	QVariantHash validators = getValidators();
	QJsonObject eodObj = validators["$$this"].toJsonObject()["equal other doc"].toObject();
	if (todo.equal_other_doc && validatorData.contains ("equal other doc"))
	{
		todo.equal_other_doc = false;
		QJsonObject eodReturn = validatorData["equal other doc"].toJsonObject();
		QJsonArray otherDocsData = eodReturn["data"].toArray();
		m_storedOtherDocsData = otherDocsData;
		if (otherDocsData.isEmpty())
		{
			ui->lE_ObjectId->setCompleter (nullptr);
			return;
		}
		QStringList completionList;
		for (auto objIter : otherDocsData)
		{
			if (!objIter.isObject())
			{
				continue;
			}
			QJsonObject aObj = objIter.toObject();
			if (aObj.contains (eodObj["remote field"].toString()))		  // ToDo: Make logic for cases where fields are not top level
			{
				completionList.append (aObj[eodObj["remote field"].toString()].toObject()["$oid"].toString());
			}
		}
		m_currentCompletionList = completionList;
		QCompleter *uiCompleter = new QCompleter (completionList);
		uiCompleter->setMaxVisibleItems(10);
		uiCompleter->setCompletionMode (QCompleter::PopupCompletion);
		ui->lE_ObjectId->setCompleter (uiCompleter);
	}
}

void ObjectIdInserter::on_pB_Validator_clicked()
{
	QVariantHash relevantData;
	if (!m_storedOtherDocsData.isEmpty())
	{
		relevantData.insert("doc", m_storedOtherDocsData);
	}
	relevantData.insert("$$me", getKey());
	relevantData.insert("current value", getValueData());
	QVariantHash returnTypeData;
	returnTypeData.insert("type", "string");
	QVariantHash userRetVal = ValidatorDialogue::showValidatorWidget(getValidators(), relevantData, returnTypeData);
	userRetVal.insert("$$me", getKey());
	emit validatorSignal(userRetVal);
}

