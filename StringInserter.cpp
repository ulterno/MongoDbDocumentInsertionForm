#include "StringInserter.h"
#include "ui_StringInserter.h"
#include <QCompleter>
#include "Elements/ValidatorDialogue.h"

StringInserter::StringInserter (const QString &keyName, const WidgetConfiguration &config, QWidget *parent)
	: QWidget (parent), DataItem (keyName), ui (new Ui::StringInserter), m_config (config)
{
	ui->setupUi (this);
	ui->label_Key->setText (keyName);
	ui->cB_Enable->setChecked (!config.disableByDefault);
	if (!config.disableByDefault)
	{
		connect (ui->cB_Enable, &QCheckBox::toggled, this,
			[this] (bool checked)
			{
			if (checked)
			{
				ui->cB_Enable->setText ("");
				ui->cB_Enable->setStyleSheet ("");
				ui->cB_Enable->setMaximumWidth (32);
			}
			else if (ui->cB_Enable->isEnabled())
			{
				ui->cB_Enable->setText ("THIS KEY WILL BE DELETED ! (If you see red)");
				ui->cB_Enable->setStyleSheet ("*:enabled{ color : Red ; }");
				ui->cB_Enable->setMaximumWidth (400);
			}
		});
	}
	connect (this, &StringInserter::validatorChanged, this,
		[this] (const QVariantHash &)
		{
		todo.validators_done = false;
		scriptControl();
	});
}

StringInserter::~StringInserter() { delete ui; }

QJsonValue StringInserter::getValueData()
{
	if (!ui->lE_Value->isEnabled())
	{
		return QJsonValue (QJsonValue::Null);
	}
	return ui->lE_Value->text();
}

bool StringInserter::setValueData (const QJsonValue &data)
{
	bool retVal;
	QString store;
	retVal = qx::cvt::detail::QxConvert_FromJson<QString>::fromJson (data, store, QString());
	if (retVal)
	{
		ui->lE_Value->setText (store);
	}
	b_failedSettingData = !retVal;
	return retVal;
}

void StringInserter::runValidator (const QVariantHash &validatorData) { runEqualOtherDoc (validatorData); }

void StringInserter::applyValidators()
{
	QVariantHash validators = getValidators();
	if (validators.isEmpty())
	{
		return;
	}
	QJsonObject valsOfThis = validators["$$this"].toJsonObject();
	if (valsOfThis.isEmpty())
	{
		return;
	}
	if (valsOfThis.contains ("case"))
	{
		QString typeCase = valsOfThis["case"].toString();
		if (typeCase == "capital")
		{
			// Restrict entered data to capital
			todo.validate_case = ToDo::VC_Caps;
			connect (ui->lE_Value, &QLineEdit::textChanged, this, &StringInserter::caseValidatorInstance, Qt::UniqueConnection);
		}
		else if (typeCase == "small")
		{
			// Restrict entered data to small
			todo.validate_case = ToDo::VC_Small;
			connect (ui->lE_Value, &QLineEdit::textChanged, this, &StringInserter::caseValidatorInstance, Qt::UniqueConnection);
		}
		else
		{
			// Remove validator
			disconnect (ui->lE_Value, &QLineEdit::textChanged, this, &StringInserter::caseValidatorInstance);
		}
	}
	if (valsOfThis.contains ("forbidden characters"))
	{
		QJsonArray forbidden_characters = valsOfThis["forbidden characters"].toArray();
		// Apply Forbidden Characters
		for (auto aChar : forbidden_characters)
		{
			todo.forbidden_characters.append (aChar.toInt());
		}
		connect (ui->lE_Value, &QLineEdit::textChanged, this, &StringInserter::forbiddenCharactersValidatorInstance, Qt::UniqueConnection);
	}
	else
	{
		disconnect (ui->lE_Value, &QLineEdit::textChanged, this, &StringInserter::forbiddenCharactersValidatorInstance);
	}
	if (valsOfThis.contains ("equal other doc"))
	{
		QJsonObject equal_other_doc_options = valsOfThis["equal other doc"].toObject();
		if (equal_other_doc_options.contains ("collection"))
		{
			// Make equal_other_doc options
			// Send document request
			QVariantHash eodRequest;
			eodRequest.insert ("equal other doc", valsOfThis["equal other doc"].toObject());
			eodRequest.insert ("$$me", getKey());
			// Set receiver on
			todo.equal_other_doc = true;
			emit validatorSignal (eodRequest);
		}
		connect (ui->lE_Value, &QLineEdit::textChanged, this, &StringInserter::equalOtherDocValidatorInstance, Qt::UniqueConnection);
	}
	else
	{
		disconnect (ui->lE_Value, &QLineEdit::textChanged, this, &StringInserter::equalOtherDocValidatorInstance);
	}
}

bool StringInserter::getCase_invalid() const
{
	return todo.indicate.case_invalid;
}

void StringInserter::setCase_invalid(bool newCase_invalid)
{
	ui->w_Indicators->setCapsValid(!newCase_invalid);
	todo.indicate.case_invalid = newCase_invalid;
}

bool StringInserter::getForbidden_characters_invalid() const
{
	return todo.indicate.forbidden_characters_invalid;
}

void StringInserter::setForbidden_characters_invalid(bool newForbidden_characters_invalid)
{
	ui->w_Indicators->setForbiddenValid(!newForbidden_characters_invalid);
	todo.indicate.forbidden_characters_invalid = newForbidden_characters_invalid;
}

bool StringInserter::getEqual_other_doc_invalid() const
{
	return todo.indicate.equal_other_doc_invalid;
}

void StringInserter::setEqual_other_doc_invalid(bool newEqual_other_doc_invalid)
{
	ui->w_Indicators->setEqualOtherDocValid(!newEqual_other_doc_invalid);
	todo.indicate.equal_other_doc_invalid = newEqual_other_doc_invalid;
}

void StringInserter::scriptControl()
{
	if (todo.validators_done)
	{
		return;
	}
	// Do Validator stuff
	applyValidators();
	todo.validators_done = true;
}

void StringInserter::forbiddenCharactersValidatorInstance (const QString &lineEditText)
{
	for (QChar fChar : todo.forbidden_characters)
	{
		if (lineEditText.contains (fChar))
		{
			setForbidden_characters_invalid(true);
			return;
		}
	}
	setForbidden_characters_invalid(false);
}

void StringInserter::caseValidatorInstance (const QString &lineEditText)
{
	switch (todo.validate_case)
	{
	case ToDo::VC_Caps:
	{
		if (lineEditText.toUpper() == lineEditText)
		{
			setCase_invalid(false);
			return;
		}
		setCase_invalid(true);
	}
	break;
	case ToDo::VC_Small:
	{
		if (lineEditText.toLower() == lineEditText)
		{
			setCase_invalid(false);
			return;
		}
		setCase_invalid(true);
	}
	break;
	default:
		return;
	}
}

void StringInserter::equalOtherDocValidatorInstance (const QString &lineEditText)
{
	if (m_currentCompletionList.contains (lineEditText))
	{
		setEqual_other_doc_invalid(false);
		return;
	}
	setEqual_other_doc_invalid(true);
}

void StringInserter::runEqualOtherDoc (const QVariantHash &validatorData)
{
	QVariantHash validators = getValidators();
	QJsonObject eodObj = validators["$$this"].toJsonObject()["equal other doc"].toObject();
	if (todo.equal_other_doc && validatorData.contains ("equal other doc"))
	{
		todo.equal_other_doc = false;
		QJsonObject eodReturn = validatorData["equal other doc"].toJsonObject();
		QJsonArray otherDocsData = eodReturn["data"].toArray();
		m_storedOtherDocsData = otherDocsData;
		if (otherDocsData.isEmpty())
		{
			ui->lE_Value->setCompleter (nullptr);
			return;
		}
		QStringList completionList;
		for (auto objIter : otherDocsData)
		{
			if (!objIter.isObject())
			{
				continue;
			}
			QJsonObject aObj = objIter.toObject();
			if (aObj.contains (eodObj["remote field"].toString()))		  // ToDo: Make logic for cases where fields are not top level
			{
				completionList.append (aObj[eodObj["remote field"].toString()].toString());
			}
		}
		m_currentCompletionList = completionList;
		QCompleter *uiCompleter = new QCompleter (completionList);
		uiCompleter->setCompletionMode (QCompleter::UnfilteredPopupCompletion);
		ui->lE_Value->setCompleter (uiCompleter);
	}
}

void StringInserter::on_pB_Validator_clicked()
{
	QVariantHash relevantData;
	if (!m_storedOtherDocsData.isEmpty())
	{
		relevantData.insert("doc", m_storedOtherDocsData);
	}
	if (!m_currentCompletionList.isEmpty())
	{
		relevantData.insert("completion", m_currentCompletionList);
	}
	relevantData.insert("$$me", getKey());
	relevantData.insert("current value", ui->lE_Value->text());
	QVariantHash returnTypeData;
	returnTypeData.insert("type", "string");
	QVariantHash userRetVal = ValidatorDialogue::showValidatorWidget(getValidators(), relevantData, returnTypeData);
	userRetVal.insert("$$me", getKey());
	emit validatorSignal(userRetVal);
}
