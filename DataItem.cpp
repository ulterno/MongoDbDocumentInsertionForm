#include "DataItem.h"

DataItem::DataItem() {}

DataItem::DataItem (const QString &keyName) : b_keyName (keyName) {}

DataItem::~DataItem() {}

QString DataItem::getKey() { return b_keyName; }

QJsonValue DataItem::getValueData() { return QJsonValue{}; }

bool DataItem::setValueData (const QJsonValue &)
{
	b_failedSettingData = true;
	return false;
}

QVariantHash DataItem::getValidators() const { return b_validators; }

void DataItem::setValidators (const QVariantHash &newValidators)
{
	b_validators.insert (newValidators);
	emit validatorChanged (b_validators);
}
