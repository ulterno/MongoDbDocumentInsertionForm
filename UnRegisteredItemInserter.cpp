#include "UnRegisteredItemInserter.h"
#include "ui_UnRegisteredItemInserter.h"

UnRegisteredItemInserter::UnRegisteredItemInserter (const QString &keyName, const WidgetConfiguration &config, QWidget *parent)
	: QWidget (parent), DataItem (keyName), ui (new Ui::UnRegisteredItemInserter)
{
	ui->setupUi (this);
	ui->label_Key->setText (keyName);
	ui->cB_Enable->setChecked (!config.disableByDefault);
	if (!config.disableByDefault)
	{
		connect (ui->cB_Enable, &QCheckBox::toggled, this,
			[this] (bool checked)
			{
			if (checked)
			{
				ui->cB_Enable->setText ("");
				ui->cB_Enable->setStyleSheet ("");
				ui->cB_Enable->setMaximumWidth (32);
			}
			else if (ui->cB_Enable->isEnabled())
			{
				ui->cB_Enable->setText ("THIS KEY WILL BE DELETED ! (If you see red)");
				ui->cB_Enable->setStyleSheet ("*:enabled{ color : Red ; }");
				ui->cB_Enable->setMaximumWidth (400);
			}
		});
	}
}

UnRegisteredItemInserter::~UnRegisteredItemInserter() { delete ui; }

QJsonValue UnRegisteredItemInserter::getValueData()
{
	if (!ui->textBrowser->isEnabled())
	{
		return QJsonValue (QJsonValue::Null);
	}
	QString store = ui->textBrowser->toPlainText();
	switch (m_currentDataType)
	{
	case QJsonValue::Object:
	{
		return QJsonDocument::fromJson (store.toUtf8()).object();
	}
	case QJsonValue::Array:
	{
		return QJsonDocument::fromJson (store.toUtf8()).array();
	}
	case QJsonValue::Bool:
	{
		bool blVal;
		if (qx::cvt::detail::QxConvert_FromString<bool>::fromString (store, blVal, QString(), 0, qx::cvt::context::ctx_type{}))
		{
			return blVal;
		}
		Q_ASSERT_X (false, "UnRegisteredItemInserter::getValueData() -> QJsonValue::Bool", "Unable to convert to Bool");
		return QJsonValue (QJsonValue::Null);
	}
	case QJsonValue::Double:
	{
		double dpVal;
		int ipVal;
		long long llVal;
		if (qx::cvt::detail::QxConvert_FromString<double>::fromString (store, dpVal, QString(), 0, qx::cvt::context::ctx_type{}))
		{
			return dpVal;
		}
		else if (qx::cvt::detail::QxConvert_FromString<int>::fromString (store, ipVal, QString(), 0, qx::cvt::context::ctx_type{}))
		{
			return ipVal;
		}
		else if (qx::cvt::detail::QxConvert_FromString<long long>::fromString (store, llVal, QString(), 0, qx::cvt::context::ctx_type{}))
		{
			return llVal;
		}
		else
		{
			Q_ASSERT_X (false, "UnRegisteredItemInserter::getValueData() -> QJsonValue::Double", "Unable to convert to anything related to double");
			return QJsonValue (QJsonValue::Null);
		}
	}
	case QJsonValue::String:
	{
		return store;
	}
	default:
		return QJsonValue (m_currentDataType);
	}
}

bool UnRegisteredItemInserter::setValueData (const QJsonValue &data)
{
	bool retVal{};
	QString store;
	if (data.isObject())
	{
		store = QString::fromUtf8 (QJsonDocument (data.toObject()).toJson());
		m_currentDataType = QJsonValue::Object;
		retVal = true;
	}
	else if (data.isArray())
	{
		store = QString::fromUtf8 (QJsonDocument (data.toArray()).toJson());
		m_currentDataType = QJsonValue::Array;
		retVal = true;
	}
	else
	{
		m_currentDataType = data.type();
		store = data.toVariant().toString();
		retVal = true;
	}
	if (retVal)
	{
		ui->textBrowser->clear();
		ui->textBrowser->append (store);
	}
	b_failedSettingData = !retVal;
	return retVal;
}

void UnRegisteredItemInserter::runValidator (const QVariantHash &validatorData) {}
