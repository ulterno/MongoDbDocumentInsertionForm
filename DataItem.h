#ifndef DATAITEM_H
#define DATAITEM_H

#include "common.h"
#include <QJsonValue>
#include <QVariantHash>

class DataItem
{
public:
	struct WidgetConfiguration
	{
		bool removable{};
		bool disableDisable{};
		bool disableByDefault{};
		bool enableDocumentId{};
		bool object_disableAllTopLevelElements{};
	};

	DataItem();
	DataItem (const QString &keyName);
	virtual ~DataItem();

	QString getKey ();
	virtual QJsonValue getValueData ();
	virtual bool setValueData (const QJsonValue &);

	QVariantHash getValidators () const;
	virtual void setValidators (const QVariantHash &newValidators);

	virtual void runValidator (const QVariantHash &validatorData) = 0;

signals:
	virtual void validatorSignal (QVariantHash info) = 0;
	virtual void validatorChanged (const QVariantHash &changedValue) = 0;

protected:
	QString b_keyName;
	bool b_failedSettingData{};

private:
	QVariantHash b_validators;
};

#endif		  // DATAITEM_H
